# This file contains metadata for your plugin.

# This file should be included when you package your plugin.

# Mandatory items:

[general]
name=Lobelia4QGIS
qgisMinimumVersion=3.16
description=Lobelia4QGIS is a botanical QGIS plugin designed to enter flora and habitats data into Lobelia database
version=0.1
author=CBNBP & MNHN
email=elsa.guilley@mnhn.fr
about=Manage Lobelia4QGIS projects and synchronize data into Lobelia database
icon=icon.png
homepage=https://lobelia-cbn.fr/
tracker=https://gitlab.com/lobelia/lobelia_4_qgis_2/-/issues
repository=https://gitlab.com/lobelia/lobelia_4_qgis_2

# End of mandatory metadata

# Recommended items:

hasProcessingProvider=no
# changelog=
# Tags are comma separated with spaces allowed
tags=botany, data input, Lobelia database, python
# Category of the plugin: Raster, Vector, Database or Web
category=Database
# experimental flag
experimental=True

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

# Since QGIS 3.8, a comma separated list of plugins to be installed
# (or upgraded) can be specified.
# Check the documentation for more information.
# plugin_dependencies=

# If the plugin can run on QGIS Server.
server=False

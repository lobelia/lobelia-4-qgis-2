import psycopg2.extensions  # For isolation levels
import psycopg2.extras

from packaging import version as v

from qgis.core import QgsSettings
from qgis.gui import QgisInterface

from ..i18n.translator import Translator

from ..constants.postgis_constants import DB_NAME, DB_HOST, DB_PORT, DB_SAVE_USER, DB_SAVE_PASSWORD
from .sqlite_connection_helper import SqliteConnectionHelper
from ..interface.postgis_connection_login_dialog import PostgisConnectionLoginDialog

# Use unicode!
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)


class PostgisConnectionHelper:
    def __init__(self, iface: QgisInterface, translator: Translator, push_message_and_save_log):
        self.iface = iface
        self.translator = translator
        self.push_message_and_save_log = push_message_and_save_log

        # Unique PostGIS connection for the entire plugin
        self.pg_conn = None
        self.pg_cur = None

    def check_pg_connection(self, connection_login_dialog: PostgisConnectionLoginDialog) -> bool:
        if not self.detect_database_connection(push_warning=False):
            if connection_login_dialog is not None:
                connection_login_dialog.initialize()
                result = connection_login_dialog.exec_()
                if result:
                    if not self.create_database_connection(connection_login_dialog):
                        return False
            if not self.detect_database_connection(push_warning=True):
                return False
            else:
                if self.pg_connection() is None:
                    return False
        else:
            if self.pg_connection() is None:
                return False
        return True

    def pg_connection(self):
        """
        Get connection to the Lobelia database.
        """
        try:
            settings = QgsSettings()
            self.pg_conn = psycopg2.connect(
                host=DB_HOST,
                database=DB_NAME,
                user=settings.value(f"/PostgreSQL/connections/{DB_NAME}/username"),
                password=settings.value(f"/PostgreSQL/connections/{DB_NAME}/password"),
                port=DB_PORT
            )
        except (Exception, psycopg2.DatabaseError):
            self.iface.messageBar().pushWarning(self.translator.tr('Lobelia database connection'),
                                                self.translator.tr('Connection to "{0}" database could not be made').format(DB_NAME))
        finally:
            if self.pg_conn:
                self.pg_cur = self.pg_conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
            return self.pg_conn

    def detect_database_connection(self, push_warning: bool) -> bool:
        """
        Check if connection to the Lobelia database does exist in QGIS settings.
        """
        settings = QgsSettings()
        settings.beginGroup(u"/PostgreSQL/connections/%s" % DB_NAME)
        if not settings.contains("database"):
            if push_warning:
                self.iface.messageBar().pushWarning(self.translator.tr('Lobelia database connection'),
                                                    self.translator.tr('No matching PostGIS connection to "{0}"').format(DB_NAME))
            return False
        else:
            # self.iface.messageBar().pushInfo('Connexion à la BDD', f'La connexion à la BDD {DB_NAME} \
            # a bien été identifiée')
            return True

    def create_database_connection(self, connection_login_dialog: PostgisConnectionLoginDialog) -> bool:
        """
        Create connection to the Lobelia database if it does not exist yet.
        """
        if not connection_login_dialog.db_password.text():
            self.iface.messageBar().pushWarning(self.translator.tr('Lobelia database connection'), self.translator.tr('Empty password !'))
            return False
        # Test connection
        pg_conn = None
        try:
            pg_conn = psycopg2.connect(
                host=DB_HOST,
                database=DB_NAME,
                user=connection_login_dialog.db_user.text(),
                password=connection_login_dialog.db_password.text(),
                port=DB_PORT
            )
            # Save connection into QGIS if working
            settings = QgsSettings()
            settings.setValue(f"/PostgreSQL/connections/{DB_NAME}/database", DB_NAME)
            settings.setValue(f"/PostgreSQL/connections/{DB_NAME}/host", DB_HOST)
            settings.setValue(f"/PostgreSQL/connections/{DB_NAME}/port", DB_PORT)
            settings.setValue(f"/PostgreSQL/connections/{DB_NAME}/sslmode", "SslPrefer")
            settings.setValue(f"/PostgreSQL/connections/{DB_NAME}/username", connection_login_dialog.db_user.text())
            settings.setValue(f"/PostgreSQL/connections/{DB_NAME}/saveUsername", DB_SAVE_USER)
            settings.setValue(f"/PostgreSQL/connections/{DB_NAME}/password", connection_login_dialog.db_password.text())
            settings.setValue(f"/PostgreSQL/connections/{DB_NAME}/savePassword", DB_SAVE_PASSWORD)
            settings.setValue(f"/PostgreSQL/connections/{DB_NAME}/geometryColumnsOnly", False)
            self.iface.messageBar().pushInfo(self.translator.tr('Lobelia database connection'),
                                             self.translator.tr('Connection to "{0}" database has successfully been created !').format(DB_NAME))
            return True
        except (Exception, psycopg2.DatabaseError):
            self.iface.messageBar().pushWarning(self.translator.tr('Lobelia database connection'), self.translator.tr('Invalid login'))
            return False
        finally:
            if pg_conn:
                pg_conn.close()

    def update_datasets_list_from_pg(self, sqlite_connection: SqliteConnectionHelper):
        try:
            sql = """SELECT id, identifiant_jdd, libelle_court, libelle, description, date_debut, date_fin
            FROM metadonnees.jeu_de_donnees
            WHERE tag like '%cbnbp%' AND publier AND (date_part('year', date_fin) >= date_part('year', CURRENT_DATE) OR date_fin IS NULL)
            ORDER BY id"""
            self.pg_cur.execute(sql)
            datasets = self.pg_cur.fetchall()
            if len(datasets) > 0:
                sqlite_connection.sqlite_cur.execute("DELETE FROM jdd;")
                to_sqlite_db = [tuple(list(line.values())) for line in datasets]
                sqlite_connection.sqlite_cur.executemany("""INSERT INTO jdd (id, uuid, libelle_court, libelle, desc, date_debut, date_fin)
                VALUES (?, ?, ?, ?, ?, ?, ?);""", to_sqlite_db)
                sqlite_connection.sqlite_conn.commit()
                log_type = self.translator.tr('Datasets update')
                log = self.translator.tr('Datasets have successfully been updated !')
                self.push_message_and_save_log(log_type, log, self.iface.messageBar().pushSuccess)
            else:
                self.iface.messageBar().pushWarning(self.translator.tr('Datasets update'),
                                                    self.translator.tr('Datasets update has not been successfull'))
            self.pg_conn.close()
            sqlite_connection.sqlite_conn.close()
        except:
            self.iface.messageBar().pushWarning(self.translator.tr('Datasets update'),
                                                self.translator.tr('Datasets update has not been successfull'))
            self.pg_conn.close()
            sqlite_connection.sqlite_conn.close()
            raise

    def update_observers_list_from_pg(self, sqlite_connection: SqliteConnectionHelper):
        sqlite_observers_already_entered = sqlite_connection.sqlite_cur.execute("SELECT DISTINCT obs_id FROM liens_releve_observateur").fetchall()
        # Get L4Q version
        version = sqlite_connection.sqlite_cur.execute('SELECT version FROM version').fetchone()[0]
        if v.parse(version) < v.parse('1.3') and len(sqlite_observers_already_entered) > 0:
            self.iface.messageBar().pushWarning(self.translator.tr('Observers update'),
                                                self.translator.tr('Observers update is impossible because observers have already been entered !'))
            return
        try:
            sql = """SELECT o.obs_id, obs_prenom AS prenom, obs_nom AS nom, email, 1 AS active
            FROM cbn_v.v_observateurs o
            LEFT JOIN cbn_v.v_user u ON u.obs_id = o.obs_id
            WHERE obs_tag like '%cbnbp%' AND email IS NOT NULL AND obs_prenom IS NOT NULL
            ORDER BY obs_nom, obs_prenom"""
            self.pg_cur.execute(sql)
            lobelia_observers = self.pg_cur.fetchall()
            if len(lobelia_observers) > 0:
                sqlite_obs_id_to_keep = []
                if v.parse(version) >= v.parse('1.3') and len(sqlite_observers_already_entered) > 0:
                    sqlite_obs_id_to_keep = [sqlite_obs['obs_id'] for sqlite_obs in sqlite_observers_already_entered
                                             if sqlite_obs['obs_id'] not in [lobelia_obs['obs_id'] for lobelia_obs in lobelia_observers]]
                if len(sqlite_obs_id_to_keep) > 0:
                    sqlite_connection.sqlite_cur.execute(f"""DELETE FROM observateurs 
                    WHERE obs_id NOT IN {tuple(sqlite_obs_id_to_keep) if len(sqlite_obs_id_to_keep) > 1 else f'({repr(sqlite_obs_id_to_keep[0])})'};""")
                else:
                    sqlite_connection.sqlite_cur.execute("DELETE FROM observateurs;")
                to_sqlite_db = [tuple(list(line.values())) for line in lobelia_observers]
                sqlite_connection.sqlite_cur.executemany("""INSERT INTO observateurs (obs_id, obs_prenom, obs_nom, obs_email, obs_turned_on)
                VALUES (?, ?, ?, ?, ?);""", to_sqlite_db)
                sqlite_connection.sqlite_conn.commit()
                log_type = self.translator.tr('Observers update')
                log = self.translator.tr('Observers have successfully been updated !')
                self.push_message_and_save_log(log_type, log, self.iface.messageBar().pushSuccess)
            else:
                self.iface.messageBar().pushWarning(self.translator.tr('Observers update'),
                                                    self.translator.tr('Observers update has not been successfull'))
            self.pg_conn.close()
            sqlite_connection.sqlite_conn.close()
        except:
            self.iface.messageBar().pushWarning(self.translator.tr('Observers update'),
                                                self.translator.tr('Observers update has not been successfull'))
            self.pg_conn.close()
            sqlite_connection.sqlite_conn.close()
            raise

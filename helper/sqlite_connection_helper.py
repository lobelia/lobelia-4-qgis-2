import sqlite3
import csv
import uuid
from urllib.request import pathname2url
from packaging import version as v
from typing import List

from qgis.core import QgsProject, QgsVectorFileWriter, QgsFieldConstraints
from qgis.gui import QgisInterface

from ..i18n.translator import Translator

from ..constants.sqlite_constants import REQUIRED_SQLITE_TABLES_LIST, REQUIRED_COLUMNS_SYNCHRO_TABLE
from .sqlite_database_cleaner import DatabaseCleaner
from ..interface.successfull_synchro_dialog import SuccessfullSynchroDialog


class SqliteConnectionHelper:
    def __init__(self, iface: QgisInterface, translator: Translator, push_message_and_save_log, plugin_dir):
        self.iface = iface
        self.translator = translator
        self.push_message_and_save_log = push_message_and_save_log

        self.plugin_dir = plugin_dir
        # Unique SQLite connection for the entire plugin
        self.sqlite_conn = None
        self.sqlite_cur = None
        # Save empty surveys (for csv_builder.py)
        self.empty_surveys = []
        # Save surveys without geom (for csv_builder.py)
        self.surveys_without_geom = []
        # Save geom layers crs (for csv_builder.py)
        self.geom_layers_crs = None

        # Successfull synchro dialog
        self.success_synchro_dialog: SuccessfullSynchroDialog = None

    def check_sqlite_db(self, project_dir) -> bool:
        """
        Check if SQLite database does exist and if it meets requirements.
        """
        # Check SQLite DB connection
        if self.sqlite_connection(project_dir) is None:
            return False

        # Check synchro table and required tables
        if not self.check_plugin_tables() or not self.check_required_tables():
            return False

        # Query to get surveys list with required fields
        get_surveys_list_query = """SELECT r.id, rel_uuid, rel_obs_syntaxon_id,
        CASE WHEN LOWER(type_rel.value) LIKE '%flore%' THEN 'Relevé flore' ELSE 'Relevé habitat' END AS type_rel,
        rel_point_uuid, rel_ligne_uuid, rel_polygon_uuid,
        CASE
            WHEN rel_point_uuid IS NOT NULL AND rel_point_uuid != '' THEN 'station_point id=' || sp.id
            WHEN rel_ligne_uuid IS NOT NULL AND rel_ligne_uuid != '' THEN 'station_ligne id=' || sl.id
            WHEN rel_polygon_uuid IS NOT NULL AND rel_polygon_uuid != '' THEN 'station_surface id=' || sf.id
        END AS geom_id,
        CASE
            WHEN rel_point_uuid IS NOT NULL AND rel_point_uuid != '' THEN sp.geom
            WHEN rel_ligne_uuid IS NOT NULL AND rel_ligne_uuid != '' THEN sl.geom
            WHEN rel_polygon_uuid IS NOT NULL AND rel_polygon_uuid != '' THEN sf.geom
        END AS geom,
        CASE
            WHEN rel_point_uuid IS NOT NULL AND rel_point_uuid != '' THEN ST_IsValidReason(sp.geom)
            WHEN rel_ligne_uuid IS NOT NULL AND rel_ligne_uuid != '' THEN ST_IsValidReason(sl.geom)
            WHEN rel_polygon_uuid IS NOT NULL AND rel_polygon_uuid != '' THEN ST_IsValidReason(sf.geom)
        END AS geom_validity,
        lro.rel_id AS lro_rel_id, GROUP_CONCAT(DISTINCT lro.obs_id) AS observers, rel_jdd_uuid, date_obs,
        CASE
            WHEN JSON_GROUP_ARRAY(DISTINCT o.obs_id) = '[null]' AND JSON_GROUP_ARRAY(DISTINCT oh.obs_hab_rel_id) = '[null]' 
            AND (JSON_GROUP_ARRAY(DISTINCT oh.obs_hab_rel_assoc_id) = '[null]' OR (JSON_GROUP_ARRAY(DISTINCT oh.obs_hab_code_typo) = '[0]' AND JSON_GROUP_ARRAY(DISTINCT oh.obs_hab_code_cite) = '[null]' AND JSON_GROUP_ARRAY(DISTINCT oh.obs_hab_nom_cite) = '[null]')) THEN False
            ELSE True
        END AS has_observations,
        JSON_GROUP_ARRAY(DISTINCT o.obs_id) AS observations, JSON_GROUP_ARRAY(DISTINCT oh.obs_hab_id) AS obs_habitats
        FROM releves r LEFT JOIN (
            SELECT * FROM listes_valeurs WHERE list_name = 'TYPE_REL_VALUES'
        ) AS type_rel ON type_rel.key = r.rel_type_rel_id
        LEFT JOIN liens_releve_observateur lro ON lro.rel_id = r.id
        LEFT JOIN observations o ON o.obs_rel_id = r.id
        LEFT JOIN obs_habitats oh ON (oh.obs_hab_rel_id = r.id OR oh.obs_hab_rel_assoc_id = r.id)
        LEFT JOIN station_point sp ON sp.uuid = r.rel_point_uuid
        LEFT JOIN station_ligne sl ON sl.uuid = r.rel_ligne_uuid
        LEFT JOIN station_surface sf ON sf.uuid = r.rel_polygon_uuid
        GROUP BY r.id
        ORDER BY r.id"""

        # Get L4Q version
        version = self.sqlite_cur.execute('SELECT version FROM version').fetchone()[0]
        # EXCEPTION : fixing L4Qv1.3 bug concerning association between a habitat survey and a flora survey # TODO : À SUPPRIMER À TERME
        # À faire au début car modifie/supprime des obs_habitats ce qui a des impacts sur self.empty_surveys et self.surveys_without_geom
        if version == '1.3':
            surveys_list = self.sqlite_cur.execute(get_surveys_list_query).fetchall()
            if not self.check_obs_habitat_v1_3(surveys_list):
                return False

        # Database cleaner
        DatabaseCleaner(self.sqlite_conn, self.sqlite_cur).clean()

        # Check surveys
        surveys_list = self.sqlite_cur.execute(get_surveys_list_query).fetchall()
        # print(surveys_list)
        if len(surveys_list) == 0:
            self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                self.translator.tr('Synchronization has not been successfull'))
            error_type = self.translator.tr('SQLite DB')
            error = self.translator.tr('There is no survey in this QGIS project')
            self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
            return False
        # Check surveys crs
        if not self.check_surveys_crs():
            return False
        # Check surveys geom validity
        if not self.check_surveys_geom_validity(surveys_list):
            return False
        # Check surveys fields
        self.empty_surveys = []
        self.surveys_without_geom = []
        for survey in surveys_list:
            self.check_survey_uuid(survey)
            if survey['has_observations']:
                if not self.check_survey_geom(survey):
                    return False
                if survey['geom_id'] is not None:
                    if not self.check_survey_observer(survey, self.get_unique_observer()) or \
                            not self.check_survey_dataset(survey, self.get_unique_dataset()) or \
                            not self.check_survey_date(survey) or \
                            not self.check_survey_obs_nom_cite(survey):
                        return False
                else:
                    self.surveys_without_geom.append(survey)
            else:
                self.empty_surveys.append(self.get_formatted_empty_survey_dict(survey))
        # Build a csv for empty surveys
        if len(self.empty_surveys) > 0:
            self.build_empty_surveys_csv(project_dir)
        # Build a csv for non-empty surveys without geom
        if len(self.surveys_without_geom) > 0:
            self.build_surveys_without_geom_csv(project_dir)

        # Check observations / obs_habitat uuid and survey
        if not self.check_observations() or not self.check_obs_habitat():
            return False

        # Check dates format
        self.check_dates_format()

        # Add mapping table (L4Q values // Lobelia values) into SQLite DB
        if v.parse(version) <= v.parse('1.2'):
            self.add_mapping_table()
        # EXCEPTION : fixing v1.3 >= L4Q >= v1.3.2 bug concerning TAILLE_COURS_EAU_VALUE "Plan d’eau" # TODO : À SUPPRIMER À TERME
        if (v.parse(version) >= v.parse('1.3')) and (v.parse(version) <= v.parse('1.3.2')):
            self.fix_taille_cours_eau_values_v1_3_v1_3_2()

        return True

    def sqlite_connection(self, project_dir):
        sqlite_db_path = project_dir / 'donnees_habitats.sqlite'
        try:
            sqlite_db_uri = f'file:{pathname2url(str(sqlite_db_path))}?mode=rw'
            self.sqlite_conn = sqlite3.connect(sqlite_db_uri, uri=True)
            self.sqlite_conn.enable_load_extension(True)
            self.sqlite_conn.load_extension("mod_spatialite")
            self.sqlite_conn.row_factory = sqlite3.Row  # Get feature values through field name
            self.sqlite_conn.text_factory = lambda b: b.decode(errors='ignore')  # Deal with "replacement character"
        except sqlite3.OperationalError:
            self.iface.messageBar().pushWarning(self.translator.tr('SQLite DB'),
                                                self.translator.tr("Connection to SQLite DB could not be made"))
            if self.sqlite_conn is not None:
                self.sqlite_conn.close()
                self.sqlite_conn = None
        finally:
            if self.sqlite_conn:
                self.sqlite_cur = self.sqlite_conn.cursor()
            return self.sqlite_conn

    def check_plugin_tables(self):
        synchro_table = self.sqlite_cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='synchro'").fetchone()
        if synchro_table:
            successfull_synchro = self.sqlite_cur.execute("SELECT success FROM synchro").fetchone()[0]
            columns_list = [row['name'] for row in self.sqlite_cur.execute("PRAGMA table_info(synchro)")]
            if columns_list != REQUIRED_COLUMNS_SYNCHRO_TABLE:
                self.sqlite_cur.execute("DROP TABLE synchro;")
                self.create_synchro_table(successfull_synchro)
            if successfull_synchro:
                self.success_synchro_dialog = SuccessfullSynchroDialog()
                self.success_synchro_dialog.initialize()
                result = self.success_synchro_dialog.exec_()
                if not result:
                    return False
        else:
            self.create_synchro_table()
        logs_table = self.sqlite_cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='plugin_logs'").fetchone()
        if not logs_table:
            self.sqlite_cur.execute("""CREATE TABLE IF NOT EXISTS plugin_logs 
            (id INTEGER PRIMARY KEY, 
            log_type TEXT,
            log TEXT,
            correction TEXT DEFAULT NULL);""")
            self.sqlite_conn.commit()
        return True

    def create_synchro_table(self, successfull_synchro=0):
        self.sqlite_cur.execute("""CREATE TABLE IF NOT EXISTS synchro
        (id INTEGER PRIMARY KEY,
        import_name TEXT,
        import_description TEXT,
        contact_id INTEGER,
        contact_name TEXT,
        occ_hab_validation INTEGER,
        validator_id INTEGER,
        validator_name TEXT,
        validation_comment TEXT,
        success INTEGER);""")
        self.sqlite_cur.execute(f"""INSERT INTO synchro (import_name, import_description, contact_id, contact_name, 
        occ_hab_validation, validator_id, validator_name, validation_comment, success)
        VALUES (NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, {str(successfull_synchro)});""")
        self.sqlite_conn.commit()

    def check_required_tables(self) -> bool:
        sqlite_tables = [row[0] for row in self.sqlite_cur.execute('SELECT name FROM sqlite_master')]
        for table in REQUIRED_SQLITE_TABLES_LIST:
            if table not in sqlite_tables:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Table "{0}" does not exist in the SQLite DB').format(table)
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
        return True

    def check_surveys_crs(self) -> bool:
        geom_layers = ['station_point', 'station_ligne', 'station_surface']
        self.geom_layers_crs = {}
        for name in geom_layers:
            crs = QgsProject.instance().mapLayersByName(name)[0].sourceCrs().authid()
            if crs.split(':')[0] != 'EPSG' or crs.split(':')[1] not in ('2154', '4326'):
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr("CRS of the layer {0} is neither 'EPSG:2154' nor 'EPSG:4326'. Please change the CRS of the layer.").format(name)
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
            self.geom_layers_crs[name] = crs.split(':')[1]
        return True

    def check_surveys_geom_validity(self, surveys_list) -> bool:
        invalid_geometries = []
        for survey in surveys_list:
            if survey['geom_id'] is not None and survey['geom'] is None:
                invalid_geometries.append(f"{survey['geom_id']} : NULL")
            if survey['geom_validity'] != 'Valid Geometry' and survey['geom_validity'] is not None:
                invalid_geometries.append(f"{survey['geom_id']} : {survey['geom_validity']}")
        if len(invalid_geometries) > 0:
            error_type = self.translator.tr('SQLite DB')
            error = self.translator.tr('Following geometries are not valid and must be corrected : ') + '\n* '.join(['', *invalid_geometries])
            self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
            self.sqlite_conn.close()
            return False
        return True

    def get_unique_observer(self):
        observers = self.sqlite_cur.execute("""SELECT DISTINCT lro.obs_id, o.obs_nom, o.obs_prenom 
        FROM liens_releve_observateur lro
        LEFT JOIN observateurs o ON o.obs_id = lro.obs_id""").fetchall()
        if len(observers) == 1:
            return {
                'obs_id': observers[0]['obs_id'],
                'obs_nom': observers[0]['obs_nom'],
                'obs_prenom': observers[0]['obs_prenom']
            }
        else:
            return None

    def get_unique_dataset(self):
        datasets = self.sqlite_cur.execute("""SELECT DISTINCT rel_jdd_uuid, jdd.libelle_court 
        FROM releves r
        LEFT JOIN jdd ON jdd.uuid = r.rel_jdd_uuid
        WHERE rel_jdd_uuid IS NOT NULL""").fetchall()
        if len(datasets) == 1:
            return {
                'jdd_uuid': datasets[0]['rel_jdd_uuid'],
                'libelle_court': datasets[0]['libelle_court']
            }
        else:
            return None

    def check_survey_uuid(self, survey):
        if survey['rel_uuid'] is None or survey['rel_uuid'] == '':
            self.sqlite_cur.execute(f"UPDATE releves SET rel_uuid = ? WHERE id=?;", (str(uuid.uuid4()), str(survey['id'])))
            self.sqlite_conn.commit()

    def check_survey_geom(self, survey) -> bool:
        if ((survey['rel_point_uuid'] is None or survey['rel_point_uuid'] == '')
                and (survey['rel_ligne_uuid'] is None or survey['rel_ligne_uuid'] == '')
                and (survey['rel_polygon_uuid'] is None or survey['rel_polygon_uuid'] == '')):
            self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                self.translator.tr('Synchronization has not been successfull'))
            error_type = self.translator.tr('SQLite DB')
            error = self.translator.tr('Survey id={0} is not linked to any geometry').format(str(survey['id']))
            self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
            self.sqlite_conn.close()
            return False
        return True

    def check_survey_observer(self, survey, unique_observer) -> bool:
        if survey['lro_rel_id'] is None or survey['observers'] is None:
            if unique_observer:
                self.sqlite_cur.execute(f"""INSERT INTO liens_releve_observateur (rel_id, obs_id) 
                VALUES ({survey['id']}, {unique_observer['obs_id']})""")
                log_type = self.translator.tr('SQLite DB')
                log = self.translator.tr('Unique observer {0} has been added for survey id={1}').format(
                    f"({str(unique_observer['obs_id'])}) {unique_observer['obs_nom']} {unique_observer['obs_prenom']}",
                    f"{str(survey['id'])} ({survey['geom_id']})")
                self.push_message_and_save_log(log_type, log, self.iface.messageBar().pushInfo)
                print(log)
            else:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Survey id={0} is not linked to any observer').format(f"{str(survey['id'])} ({survey['geom_id']})")
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
        return True

    def check_survey_dataset(self, survey, unique_dataset) -> bool:
        if survey['rel_jdd_uuid'] is None or survey['rel_jdd_uuid'] == '':
            if unique_dataset:
                self.sqlite_cur.execute(f"""UPDATE releves SET rel_jdd_uuid = '{unique_dataset['jdd_uuid']}' 
                WHERE id = {survey['id']}""")
                log_type = self.translator.tr('SQLite DB')
                log = self.translator.tr('Unique dataset "{0}" has been added for survey id={1}').format(
                    unique_dataset['libelle_court'], f"{str(survey['id'])} ({survey['geom_id']})")
                self.push_message_and_save_log(log_type, log, self.iface.messageBar().pushInfo)
                print(log)
            else:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Survey id={0} is not linked to any dataset').format(f"{str(survey['id'])} ({survey['geom_id']})")
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
        return True

    def check_survey_date(self, survey) -> bool:
        if survey['date_obs'] is None or survey['date_obs'] == '':
            self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                self.translator.tr('Synchronization has not been successfull'))
            error_type = self.translator.tr('SQLite DB')
            error = self.translator.tr('Survey id={0} is not linked to any date').format(f"{str(survey['id'])} ({survey['geom_id']})")
            self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
            self.sqlite_conn.close()
            return False
        return True

    def check_survey_obs_nom_cite(self, survey) -> bool:
        # Check observations
        observations_list = self.sqlite_cur.execute(f"""SELECT obs_id, nom_cite FROM observations
        WHERE obs_rel_id = {survey['id']}""").fetchall()
        for obs in observations_list:
            if obs['nom_cite'] is None or obs['nom_cite'] == '':
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Observation obs_id={0} (survey id={1}) does not have nom_cite').format(
                    str(obs['obs_id']), f"{str(survey['id'])} / {survey['geom_id']}")
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
            # Check obs_habitats
        obs_habitat_list = self.sqlite_cur.execute(f"""SELECT obs_hab_id, obs_hab_rel_id, obs_hab_rel_assoc_id, 
        obs_hab_code_typo, obs_hab_code_cite, obs_hab_nom_cite
        FROM obs_habitats
        WHERE obs_hab_rel_id = {survey['id']} OR obs_hab_rel_assoc_id = {survey['id']}""").fetchall()
        for obs_hab in obs_habitat_list:
            if obs_hab['obs_hab_code_typo'] is None or obs_hab['obs_hab_code_typo'] == '':
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Obs_habitat obs_hab_id={0} (survey id={1}) does not have code_typo').format(
                    str(obs_hab['obs_hab_id']), f"{str(survey['id'])} / {survey['geom_id']}")
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
            if (obs_hab['obs_hab_nom_cite'] is None or obs_hab['obs_hab_nom_cite'] == '') and not \
                    (obs_hab['obs_hab_rel_id'] is None and obs_hab['obs_hab_rel_assoc_id'] is not None
                     and obs_hab['obs_hab_code_typo'] == 0 and (obs_hab['obs_hab_code_cite'] is None or obs_hab['obs_hab_code_cite'] == '')):
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Obs_habitat obs_hab_id={0} (survey id={1}) does not have nom_cite').format(
                    str(obs_hab['obs_hab_id']), f"{str(survey['id'])} / {survey['geom_id']}")
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
        return True

    @staticmethod
    def get_fieldnames(dictionary):
        """Get keys names from a dict."""
        return [key for key in dictionary.keys()]

    def get_formatted_empty_survey_dict(self, survey):
        keys = self.sqlite_cur.execute("SELECT name FROM pragma_table_info('releves')").fetchall()
        survey_values = self.sqlite_cur.execute(f"SELECT * FROM releves r WHERE r.id = {survey['id']}").fetchone()
        survey_dict = {'geom_id': survey['geom_id']}
        for key in keys:
            survey_dict[key['name']] = survey_values[key['name']]
        return survey_dict

    def build_empty_surveys_csv(self, project_dir):
        with open(str(project_dir / 'Releves_vides_non_integres.csv'), 'w') as output_file:
            fieldnames = self.get_fieldnames(self.empty_surveys[0])
            writer = csv.DictWriter(output_file, fieldnames=fieldnames, delimiter=';')
            writer.writeheader()
            writer.writerows(self.empty_surveys)
        log_type = self.translator.tr('SQLite DB')
        log = 'Certains relevés sans observations ni obs_habitats seront supprimés (voir le fichier Releves_vides_non_integres.csv généré dans le dossier L4Q)'
        self.push_message_and_save_log(log_type, log, self.iface.messageBar().pushWarning)
        print(log)

    def build_surveys_without_geom_csv(self, project_dir):
        surveys_without_geom_id = [survey['id'] for survey in self.surveys_without_geom]
        self.surveys_without_geom = [dict(survey) for survey in self.surveys_without_geom]  # Nécessaire pour ajouter une key aux relevés
        with open(str(project_dir / 'Releves_sans_geometrie_NON_VIDES.csv'), 'w') as output_file:
            output_file.write('Relevés sans géométrie NON VIDES' + '\n\n')
            for survey in self.surveys_without_geom:
                survey_observations = self.sqlite_cur.execute(
                    f"""SELECT obs_id, cd_nom, nom_cite, cd_nom_valide, nom_valide FROM observations
                    WHERE obs_rel_id = {survey['id']}
                    ORDER BY obs_id""").fetchall()
                survey_obs_habitats = self.sqlite_cur.execute(
                    f"""SELECT obs_hab_id, obs_hab_rel_id, obs_hab_rel_assoc_id,
                    obs_hab_code_cite, obs_hab_nom_cite, syntaxon_code_phyto_retenu, syntaxon_nom_retenu
                    FROM obs_habitats
                    WHERE obs_hab_rel_id = {survey['id']} OR obs_hab_rel_assoc_id = {survey['id']}
                    ORDER BY obs_hab_id""").fetchall()
                if len(survey_observations) > 0 or len(survey_obs_habitats) > 0:
                    output_file.write(f"{survey['type_rel']} id = {survey['id']}" + '\n')
                    if len(survey_observations) > 0:
                        output_file.write('observations' + '\n')
                        for obs in survey_observations:
                            data = [obs['obs_id'], obs['cd_nom'], obs['nom_cite'], obs['cd_nom_valide'], obs['nom_valide']]
                            data = [d if d is not None else '' for d in data]
                            line = ';'.join(str(d) for d in data) + '\n'
                            output_file.write(line)
                    if len(survey_obs_habitats) > 0:
                        output_file.write('obs_habitats' + '\n')
                        for obs_hab in survey_obs_habitats:
                            data = [obs_hab['obs_hab_id'], obs_hab['obs_hab_code_cite'], obs_hab['obs_hab_nom_cite'],
                                    obs_hab['syntaxon_code_phyto_retenu'], obs_hab['syntaxon_nom_retenu']]
                            if obs_hab['obs_hab_rel_id'] is not None and obs_hab['obs_hab_rel_assoc_id'] is not None:
                                if survey['type_rel'] == 'Relevé habitat':
                                    assoc_survey = self.sqlite_cur.execute(f"""SELECT r.id,
                                        CASE
                                            WHEN rel_point_uuid IS NOT NULL AND rel_point_uuid != '' THEN 'station_point id=' || sp.id
                                            WHEN rel_ligne_uuid IS NOT NULL AND rel_ligne_uuid != '' THEN 'station_ligne id=' || sl.id
                                            WHEN rel_polygon_uuid IS NOT NULL AND rel_polygon_uuid != '' THEN 'station_surface id=' || sf.id
                                        END AS geom_id
                                        FROM releves r
                                        LEFT JOIN station_point sp ON sp.uuid = r.rel_point_uuid
                                        LEFT JOIN station_ligne sl ON sl.uuid = r.rel_ligne_uuid
                                        LEFT JOIN station_surface sf ON sf.uuid = r.rel_polygon_uuid
                                        WHERE r.id = {obs_hab['obs_hab_rel_assoc_id']}""").fetchone()
                                    data.append(f"=> Lié au relevé flore id={obs_hab['obs_hab_rel_assoc_id']}" + (f" / {assoc_survey['geom_id']}" if assoc_survey['geom_id'] is not None else ''))
                                    # Do not delete obs_habitat if linked to a retained survey ! Only delete the link
                                    if obs_hab['obs_hab_rel_assoc_id'] not in surveys_without_geom_id:
                                        survey[f"obs_hab_id={obs_hab['obs_hab_id']} SQL"] = f"UPDATE obs_habitats SET obs_hab_rel_id = NULL WHERE obs_hab_id = {obs_hab['obs_hab_id']}"
                                if survey['type_rel'] == 'Relevé flore':
                                    assoc_survey = self.sqlite_cur.execute(f"""SELECT r.id,
                                        CASE
                                            WHEN rel_point_uuid IS NOT NULL AND rel_point_uuid != '' THEN 'station_point id=' || sp.id
                                            WHEN rel_ligne_uuid IS NOT NULL AND rel_ligne_uuid != '' THEN 'station_ligne id=' || sl.id
                                            WHEN rel_polygon_uuid IS NOT NULL AND rel_polygon_uuid != '' THEN 'station_surface id=' || sf.id
                                        END AS geom_id
                                        FROM releves r
                                        LEFT JOIN station_point sp ON sp.uuid = r.rel_point_uuid
                                        LEFT JOIN station_ligne sl ON sl.uuid = r.rel_ligne_uuid
                                        LEFT JOIN station_surface sf ON sf.uuid = r.rel_polygon_uuid
                                        WHERE r.id = {obs_hab['obs_hab_rel_id']}""").fetchone()
                                    data.append(f"=> Lié au relevé habitat id={obs_hab['obs_hab_rel_id']}" + (f" / {assoc_survey['geom_id']}" if assoc_survey['geom_id'] is not None else ''))
                                    # Do not delete obs_habitat if linked to a retained survey ! Only delete the link
                                    if obs_hab['obs_hab_rel_id'] not in surveys_without_geom_id:
                                        survey[f"obs_hab_id={obs_hab['obs_hab_id']} SQL"] = f"UPDATE obs_habitats SET obs_hab_rel_assoc_id = NULL WHERE obs_hab_id = {obs_hab['obs_hab_id']}"
                            data = [d if d is not None else '' for d in data]
                            line = ';'.join(str(d) for d in data) + '\n'
                            output_file.write(line)
                    output_file.write('\n')
        log_type = self.translator.tr('SQLite DB')
        log = 'Certains relevés sans géométrie NON VIDES seront supprimés (voir le fichier Releves_sans_geometrie_NON_VIDES.csv généré dans le dossier L4Q)'
        self.push_message_and_save_log(log_type, log, self.iface.messageBar().pushWarning)
        print(log)

    def check_observations(self) -> bool:
        observations_list = self.sqlite_cur.execute("""SELECT obs_id, obs_uuid, obs_rel_id, r.id AS rel_id, cd_nom, nom_cite
        FROM observations o
        LEFT JOIN releves r ON r.id = o.obs_rel_id""").fetchall()
        for obs in observations_list:
            if obs['obs_uuid'] is None or obs['obs_uuid'] == '':
                self.sqlite_cur.execute('UPDATE observations SET obs_uuid = ? WHERE obs_id=?;', (str(uuid.uuid4()), str(obs['obs_id'])))
                self.sqlite_conn.commit()
            if obs['obs_rel_id'] is None or obs['obs_rel_id'] == '':
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Observation obs_id={0} is not linked to any survey').format(str(obs['obs_id']))
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
            if obs['rel_id'] != obs['obs_rel_id']:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Observation obs_id={0} is linked to flora survey id={1} that does not exist').format(
                    str(obs['obs_id']), str(obs['obs_rel_id']))
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
            # if obs['nom_cite'] is None or obs['nom_cite'] == '':
            #     self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
            #                                         self.translator.tr('Synchronization has not been successfull'))
            #     error_type = self.translator.tr('SQLite DB')
            #     error = self.translator.tr('Observation obs_id={0} does not have nom_cite').format(str(obs['obs_id']))
            #     self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
            #     self.sqlite_conn.close()
            #     return False
            # if (obs['cd_nom'] is None or obs['cd_nom'] == '' or obs['cd_nom'] == 0) and obs['nom_cite'] != 'Taxon non indexé':
            #     self.iface.messageBar().pushWarning(self.translator.tr('SQLite DB'),
            #                                         self.translator.tr('Observation obs_id={0} does not have cd_nom => It may need to be reindexed !').format(str(obs['obs_id'])))
        return True

    def check_obs_habitat(self) -> bool:
        obs_habitat_list = self.sqlite_cur.execute("""SELECT obs_hab_id, obs_hab_uuid, obs_hab_rel_id, r1.id AS rel_id, 
        obs_hab_rel_assoc_id, r2.id AS rel_assoc_id, obs_hab_code_typo, obs_hab_code_cite, obs_hab_nom_cite
        FROM obs_habitats oh
        LEFT JOIN releves r1 ON r1.id = oh.obs_hab_rel_id
        LEFT JOIN releves r2 ON r2.id = oh.obs_hab_rel_assoc_id""").fetchall()
        for obs_hab in obs_habitat_list:
            if obs_hab['obs_hab_uuid'] is None or obs_hab['obs_hab_uuid'] == '':
                self.sqlite_cur.execute('UPDATE obs_habitats SET obs_hab_uuid = ? WHERE obs_hab_id=?;', (str(uuid.uuid4()), str(obs_hab['obs_hab_id'])))
                self.sqlite_conn.commit()
            if (obs_hab['obs_hab_rel_id'] is None or obs_hab['obs_hab_rel_id'] == '') and \
               (obs_hab['obs_hab_rel_assoc_id'] is None or obs_hab['obs_hab_rel_assoc_id'] == ''):
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Obs_habitat obs_hab_id={0} is not linked to any survey').format(str(obs_hab['obs_hab_id']))
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
            if obs_hab['rel_id'] != obs_hab['obs_hab_rel_id']:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Obs_habitat obs_hab_id={0} is linked to habitat survey id={1} that does not exist').format(
                    str(obs_hab['obs_hab_id']), str(obs_hab['obs_hab_rel_id']))
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
            if obs_hab['rel_assoc_id'] != obs_hab['obs_hab_rel_assoc_id'] and not \
                    (obs_hab['obs_hab_rel_id'] is None and obs_hab['obs_hab_rel_assoc_id'] is not None
                     and obs_hab['obs_hab_code_typo'] == 0 and obs_hab['obs_hab_code_cite'] is None):
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('SQLite DB')
                error = self.translator.tr('Obs_habitat obs_hab_id={0} is linked to flora survey id={1} that does not exist').format(
                    str(obs_hab['obs_hab_id']), str(obs_hab['obs_hab_rel_assoc_id']))
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                self.sqlite_conn.close()
                return False
            # if obs_hab['obs_hab_code_typo'] is None or obs_hab['obs_hab_code_typo'] == '':
            #     self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
            #                                         self.translator.tr('Synchronization has not been successfull'))
            #     error_type = self.translator.tr('SQLite DB')
            #     error = self.translator.tr('Obs_habitat obs_hab_id={0} does not have code_typo').format(str(obs_hab['obs_hab_id']))
            #     self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
            #     self.sqlite_conn.close()
            #     return False
            # if (obs_hab['obs_hab_nom_cite'] is None or obs_hab['obs_hab_nom_cite'] == '') and not \
            #         (obs_hab['obs_hab_rel_id'] is None and obs_hab['obs_hab_rel_assoc_id'] is not None
            #          and obs_hab['obs_hab_code_typo'] == 0 and obs_hab['obs_hab_code_cite'] is None):
            #     self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
            #                                         self.translator.tr('Synchronization has not been successfull'))
            #     error_type = self.translator.tr('SQLite DB')
            #     error = self.translator.tr('Obs_habitat obs_hab_id={0} does not have nom_cite').format(str(obs_hab['obs_hab_id']))
            #     self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
            #     self.sqlite_conn.close()
            #     return False
        return True

    def check_dates_format(self):
        # "releves" table
        dates_to_update = self.sqlite_cur.execute("""SELECT * FROM releves 
        WHERE INSTR(date_obs, '/') OR INSTR(date_obs_max, '/')""").fetchall()
        if len(dates_to_update) > 0:
            self.sqlite_cur.execute("""UPDATE releves 
            SET date_obs = substr(date_obs, 7) || '-' || substr(date_obs, 4, 2) || '-' || substr(date_obs, 1, 2)
            WHERE INSTR(date_obs, '/')""")
            self.sqlite_cur.execute("""UPDATE releves 
            SET date_obs_max = substr(date_obs_max, 7) || '-' || substr(date_obs_max, 4, 2) || '-' || substr(date_obs_max, 1, 2)
            WHERE INSTR(date_obs_max, '/')""")
            self.sqlite_conn.commit()
            log_type = self.translator.tr('SQLite DB')
            log = self.translator.tr('The format of some dates into the "releves" table had to be updated')
            self.push_message_and_save_log(log_type, log, self.iface.messageBar().pushInfo)
            print(log)

    def add_mapping_table(self):
        self.sqlite_cur.execute('CREATE TABLE IF NOT EXISTS listes_valeurs_lobelia (id INTEGER PRIMARY KEY, list_name TEXT, key INTEGER, L4Q_value, lobelia_value);')
        to_sqlite_db: List[tuple]
        with open(str(self.plugin_dir / 'resources/mapping_listes_valeurs_L4Qv1.2-Lobelia.csv'), 'r') as listes_valeurs_lobelia:
            data = csv.DictReader(listes_valeurs_lobelia, delimiter=';')  # csv.DictReader uses first line in file for column headings by default
            to_sqlite_db = [tuple(list(line.values())) for line in data]
        self.sqlite_cur.execute('DELETE FROM listes_valeurs_lobelia;')
        self.sqlite_cur.executemany('INSERT INTO listes_valeurs_lobelia (id, list_name, key, L4Q_value, lobelia_value) VALUES (?, ?, ?, ?, ?);', to_sqlite_db)
        self.sqlite_conn.commit()
        print('Ajout de la table de correspondance des listes de valeurs')

    def check_obs_habitat_v1_3(self, surveys_list):
        print_message = False
        surveys_with_rel_obs_syntaxon = [survey for survey in surveys_list if survey['type_rel'] == 'Relevé flore'
                                         and survey['rel_obs_syntaxon_id'] is not None]
        for survey in surveys_with_rel_obs_syntaxon:
            syntaxon = self.sqlite_cur.execute(f"""SELECT obs_hab_id, obs_hab_rel_id, obs_hab_rel_assoc_id 
            FROM obs_habitats WHERE obs_hab_id = {survey['rel_obs_syntaxon_id']}""").fetchone()
            if syntaxon is None and (survey['has_observations'] and survey['geom_id'] is not None):
                if print_message:
                    log_type = 'L4Q v1.3'
                    log = self.translator.tr('Modifications has been made to obs_habitats table')
                    self.push_message_and_save_log(log_type, log, self.iface.messageBar().pushInfo)
                    print(log_type + ' : ' + log)
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = 'L4Q v1.3'
                error = self.translator.tr('Survey id={0} refers a syntaxon through rel_obs_syntaxon_id={1} that does not exist').format(
                    f"{str(survey['id'])} ({survey['geom_id']})", str(survey['rel_obs_syntaxon_id']))
                self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                return False
            if syntaxon['obs_hab_rel_assoc_id'] != survey['id']:
                if syntaxon['obs_hab_rel_assoc_id'] is None or syntaxon['obs_hab_rel_assoc_id'] == '':
                    self.sqlite_cur.execute(f"""UPDATE obs_habitats SET obs_hab_rel_assoc_id = {survey['id']}
                    WHERE obs_hab_id = {survey['rel_obs_syntaxon_id']};""")
                    self.sqlite_conn.commit()
                    print_message = True
                else:
                    if print_message:
                        log_type = 'L4Q v1.3'
                        log = self.translator.tr('Modifications has been made to obs_habitats table')
                        self.push_message_and_save_log(log_type, log, self.iface.messageBar().pushInfo)
                        print(log_type + ' : ' + log)
                    self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                        self.translator.tr('Synchronization has not been successfull'))
                    error_type = 'L4Q v1.3'
                    error = self.translator.tr('For obs_habitat obs_hab_id={0}, obs_hab_rel_assoc_id should be equal to {1}').format(
                        str(survey['rel_obs_syntaxon_id']), str(survey['id']))
                    self.push_message_and_save_log(error_type, error, self.iface.messageBar().pushWarning)
                    return False
        if print_message:
            log_type = 'L4Q v1.3'
            log = self.translator.tr('Modifications has been made to obs_habitats table')
            self.push_message_and_save_log(log_type, log, self.iface.messageBar().pushInfo)
            print(log_type + ' : ' + log)
        return True

    def fix_taille_cours_eau_values_v1_3_v1_3_2(self):
        wrong_value = self.sqlite_cur.execute("""SELECT * FROM listes_valeurs
        WHERE value = 'Plan d’eau' OR import_value = 'Plan d’eau'""").fetchall()
        if len(wrong_value) > 0:
            self.sqlite_cur.execute("""UPDATE listes_valeurs
            SET value = 'Plan d''eau', import_value = 'Plan d''eau'
            WHERE value = 'Plan d’eau' OR import_value = 'Plan d’eau'""")
            log = 'Correction de la valeur "Plan d\'eau" dans la liste TAILLE_COURS_EAU_VALUES'
            self.sqlite_cur.execute(f"""INSERT INTO plugin_logs (log_type, log) VALUES ('BDD SQLite', '{log.replace("'", "''")}');""")
            self.sqlite_conn.commit()
            print(log)

    def update_station_surface_layer_from_carto_habitats_shp(self, project, project_dir):
        # Check SQLite DB connection
        if self.sqlite_connection(project_dir) is None:
            return False
        # Check carto_habitats Shapefile
        carto_habitats_layer = project.mapLayersByName("carto_habitats")
        if len(carto_habitats_layer) == 0:
            self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                self.translator.tr('station_surface update has not been successfull'))
            self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                self.translator.tr('carto_habitats layer has not been found in the QGIS project'))
            return
        else:
            carto_habitats_layer = carto_habitats_layer[0]
        # Fix carto_habitats
        carto_habitats_layer.startEditing()
        carto_habitats_layer.setFieldConstraint(0, QgsFieldConstraints.ConstraintNotNull, QgsFieldConstraints.ConstraintStrengthHard)
        id_default_value = carto_habitats_layer.defaultValueDefinition(0)
        id_default_value.setApplyOnUpdate(False)
        carto_habitats_layer.setDefaultValueDefinition(0, id_default_value)
        carto_habitats_layer.updateFields()
        for i, feature in enumerate(carto_habitats_layer.getFeatures()):
            feature.setAttribute('id', i+1)
            carto_habitats_layer.updateFeature(feature)
        carto_habitats_layer.setFieldConstraint(0, QgsFieldConstraints.ConstraintUnique, QgsFieldConstraints.ConstraintStrengthHard)
        carto_habitats_layer.updateFields()
        carto_habitats_layer.commitChanges()
        # Export carto_habitats Shapefile to SQLite format
        carto_habitats_sqlite_file_path = project_dir / 'carto_habitats_sqlite.sqlite'
        transform_context = project.transformContext()
        save_options = QgsVectorFileWriter.SaveVectorOptions()
        save_options.layerName = carto_habitats_layer.name()
        save_options.driverName = 'SQLite'
        save_options.layerOptions = ['SPATIALITE=YES']
        error = QgsVectorFileWriter.writeAsVectorFormatV3(layer=carto_habitats_layer,
                                                          fileName=str(carto_habitats_sqlite_file_path),
                                                          transformContext=transform_context,
                                                          options=save_options)
        if error[0] != QgsVectorFileWriter.NoError:
            self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                self.translator.tr('station_surface update has not been successfull'))
            self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'), error)
        # Attach the two SQLite databases
        self.sqlite_cur.execute(f"ATTACH DATABASE '{str(carto_habitats_sqlite_file_path)}' AS carto_habitats_db")
        # Check on station_surface
        station_surface_entities = self.sqlite_cur.execute("SELECT * FROM station_surface ORDER BY id").fetchall()
        for entity in station_surface_entities:
            sql = f"""SELECT sf.id AS sf_id, sf.uuid AS sf_uuid, sf.geom AS sf_geom, ch.id AS polygon_id, ST_GeomFromWKB(ch.GEOMETRY, 2154) AS polygon_geom
            FROM station_surface sf
            JOIN carto_habitats_db.carto_habitats ch ON ST_Intersects(ST_GeomFromWKB(ch.GEOMETRY, 2154), sf.geom)
            WHERE sf.id = {entity['id']}"""
            carto_habitats_polygons = self.sqlite_cur.execute(sql).fetchall()
            if len(carto_habitats_polygons) == 0:
                self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                    self.translator.tr('station_surface update has not been successfull'))
                self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                    self.translator.tr('station_surface entity id={0} has no equivalent polygon in carto_habitats !').format(entity['id']))
                return
            if len(carto_habitats_polygons) > 1:
                carto_habitats_ids = [polygon['polygon_id'] for polygon in carto_habitats_polygons]
                self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                    self.translator.tr('station_surface update has not been successfull'))
                self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                    self.translator.tr('station_surface entity id={0} straddles several polygons of carto_habitats : id = {1}')
                                                    .format(entity['id'], str(carto_habitats_ids).replace('[', '').replace(']', '')))
                return
        # Check on carto_habitats
        carto_habitats_polygons = self.sqlite_cur.execute("SELECT * FROM carto_habitats_db.carto_habitats ORDER BY id").fetchall()
        for polygon in carto_habitats_polygons:
            sql = f"""SELECT ch.id AS polygon_id, ST_GeomFromWKB(ch.GEOMETRY, 2154) AS polygon_geom, sf.id AS sf_id, sf.uuid AS sf_uuid, sf.geom AS sf_geom
            FROM carto_habitats_db.carto_habitats ch
            JOIN station_surface sf ON ST_Intersects(ST_GeomFromWKB(ch.GEOMETRY, 2154), sf.geom)
            WHERE ch.id = {polygon['id']}"""
            station_surface_entities = self.sqlite_cur.execute(sql).fetchall()
            if len(station_surface_entities) > 1:
                station_surface_ids = [entity['sf_id'] for entity in station_surface_entities]
                self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                    self.translator.tr('station_surface update has not been successfull'))
                self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                    self.translator.tr('carto_habitats polygon id={0} contains several station_surface entities : id = {1}')
                                                    .format(polygon['id'], str(station_surface_ids).replace('[', '').replace(']', '')))
                return
            if len(station_surface_entities) == 0:
                self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                    self.translator.tr('station_surface update has not been successfull'))
                self.iface.messageBar().pushWarning(self.translator.tr('station_surface update'),
                                                    self.translator.tr('carto_habitats polygon id={0} contains no station_surface entity').format(polygon['id']))
                return
        # Replace station_surface entities with carto_habitats polygons if everything is ok
        sql = """WITH data_to_update AS (
            SELECT sf.id AS sf_id, sf.uuid AS sf_uuid, sf.geom AS sf_geom, ch.id AS polygon_id, ST_GeomFromWKB(ch.GEOMETRY, 2154) AS polygon_geom
            FROM station_surface sf
            LEFT JOIN carto_habitats_db.carto_habitats ch ON ST_Intersects(ST_GeomFromWKB(ch.GEOMETRY, 2154), sf.geom)
        )
        UPDATE station_surface
        SET geom = d.polygon_geom
        FROM data_to_update d
        WHERE station_surface.uuid = d.sf_uuid"""
        self.sqlite_cur.execute(sql)
        self.sqlite_conn.close()
        # Remove carto_habitats_sqlite SQLite
        carto_habitats_sqlite_file_path.unlink()
        self.iface.messageBar().pushSuccess(self.translator.tr('station_surface update'),
                                            self.translator.tr('station_surface has successfully been updated !'))

from typing import List

from qgis.gui import QgisInterface
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QWidget


class ActionHelper:
    def __init__(self, iface: QgisInterface):
        self.iface = iface

    def add_action(
            self,
            actions: List,
            icon_path: str,
            text: str,
            callback,
            enabled_flag: bool = True,
            add_to_menu: bool = True,
            add_to_toolbar: bool = True,
            status_tip: str = None,
            whats_this: str = None,
            menu: str = 'Blank Action',
            parent: QWidget = None) -> QAction:
        """Configure and create an action (processing tool).

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            # Adds plugin icon to Plugins toolbar
            self.iface.addToolBarIcon(action)

        if add_to_menu:
            self.iface.addPluginToDatabaseMenu(menu, action)

        actions.append(action)

        return action

    def remove_actions(self, actions: List[QAction], menu: str = 'Blank Action'):
        """
        Remove action(s).
        """
        for action in actions:
            self.iface.removePluginDatabaseMenu(menu, action)
            self.iface.removeToolBarIcon(action)

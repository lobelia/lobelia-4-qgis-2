# Required SQLite tables list for import pivot
REQUIRED_SQLITE_TABLES_LIST = ['releves', 'observations', 'obs_habitats', 'liens_releve_strate',
                               'liens_releve_influence', 'liens_releve_observateur', 'liens_obs_habitat']
# Required columns for synchro table
REQUIRED_COLUMNS_SYNCHRO_TABLE = ['id', 'import_name', 'import_description', 'contact_id', 'contact_name',
                                  'occ_hab_validation', 'validator_id', 'validator_name', 'validation_comment',
                                  'success']
# Validators
VALIDATORS_LIST = ['BELLENFANT Sylvain', 'BILLOD Guillaume', 'CAUSSE Gaël', 'FERNEZ Thierry']

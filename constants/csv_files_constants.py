# Saving directory for csv files
SERVER_PATH = 'N:'
COMMON_CSV_DIR_PATH = ['_Public_commun', 'Bases_Lobelia4Qgis', 'Z_NE_PAS_TOUCHER_Synchro']
# csv files names
SOURCE_CSV_NAME = 'source_import.csv'
OBSERVERS_CSV_NAME = 'observateurs.csv'
ORGANIZATIONS_CSV_NAME = 'organismes.csv'
DATASETS_CSV_NAME = 'jeu_de_donnees.csv'
FRAMEWORKS_CSV_NAME = 'cadre_acquisition.csv'
SURVEYS_CSV_NAME = 'releves.csv'
OBSERVATIONS_CSV_NAME = 'observations.csv'
OBS_HABITAT_CSV_NAME = 'obs_habitat.csv'

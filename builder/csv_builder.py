import csv
import re
import unicodedata
import json
from pathlib import Path
from datetime import datetime, date
from packaging import version as v

from qgis.gui import QgisInterface
from qgis.core import Qgis
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtWidgets import QProgressBar

from ..i18n.translator import Translator

from ..constants.csv_files_constants import COMMON_CSV_DIR_PATH, SOURCE_CSV_NAME, OBSERVERS_CSV_NAME, ORGANIZATIONS_CSV_NAME, \
    DATASETS_CSV_NAME, FRAMEWORKS_CSV_NAME, SURVEYS_CSV_NAME, OBSERVATIONS_CSV_NAME, OBS_HABITAT_CSV_NAME
from ..helper.sqlite_connection_helper import SqliteConnectionHelper
from ..helper.postgis_connection_helper import PostgisConnectionHelper
from ..interface.synchronize_data_dialog import SynchronizeDataDialog


def strip_accents(string):
    """Remove accents from string and replace it with the closest possible representation."""
    return ''.join(c for c in unicodedata.normalize('NFD', string) if unicodedata.category(c) != 'Mn')


class CsvBuilder:
    def __init__(self, iface: QgisInterface, translator: Translator, project_dir, plugin_dir, save_log,
                 push_message_and_save_log, sqlite_connection: SqliteConnectionHelper,
                 pg_connection: PostgisConnectionHelper, sync_dialog: SynchronizeDataDialog):
        self.iface = iface
        self.translator = translator
        self.save_log = save_log
        self.push_message_and_save_log = push_message_and_save_log

        # Get SQLite connection
        self.sqlite_conn = sqlite_connection.sqlite_conn
        self.sqlite_cur = sqlite_connection.sqlite_cur
        self.logs_to_save = []
        # Get another sqlite connection (called "synchro_connection") in order to execute synchro treatments but not commit in case of failure
        synchro_connection = SqliteConnectionHelper(self.iface, self.translator, self.push_message_and_save_log, plugin_dir)
        synchro_connection.sqlite_connection(project_dir)
        self.synchro_conn = synchro_connection.sqlite_conn
        self.synchro_cur = synchro_connection.sqlite_cur
        # Get Lobelia PostGIS connection
        self.pg_conn = pg_connection.pg_conn
        self.pg_cur = pg_connection.pg_cur
        # Get empty surveys
        self.empty_surveys = sqlite_connection.empty_surveys
        # Get surveys without geom
        self.surveys_without_geom = sqlite_connection.surveys_without_geom
        # Get L4Q version
        self.version = self.sqlite_cur.execute('SELECT version FROM version').fetchone()[0]
        # Get synchronize data dialog
        self.sync_dialog = sync_dialog
        # Saving directory path for csv files
        self.common_csv_dir = self.sync_dialog.common_csv_dir if not self.sync_dialog.server_path.isEnabled() else Path(self.sync_dialog.server_path.text(), *COMMON_CSV_DIR_PATH)
        self.csv_dir = None
        # Save selected contact from sync dialog
        self.contact = self.sync_dialog.contact.currentData()
        # Save SQLite uuid's of datasets of interest
        self.jdd_uuid_list = []
        # self.jdd_uuid_list.remove(None)  # À SUPPRIMER car impossible lorsque la vérif sur les relevés sera en place
        # Get geom layers crs
        self.geom_layers_crs = sqlite_connection.geom_layers_crs

    def save_synchro_information(self):
        """Save form information for next trial"""
        import_name = self.sync_dialog.import_name.text() if self.sync_dialog.import_name.text() else ''
        import_description = self.sync_dialog.import_description.toPlainText() if self.sync_dialog.import_description.toPlainText() else ''
        contact_id = self.contact[0] if self.sync_dialog.contact.currentIndex() != 0 else ''
        contact_firstname = self.contact[2] if self.sync_dialog.contact.currentIndex() != 0 else ''
        contact_lastname = self.contact[1] if self.sync_dialog.contact.currentIndex() != 0 else ''
        occ_hab_validation = self.sync_dialog.occ_hab_validation.currentData() if self.sync_dialog.occ_hab_validation.currentIndex() != 0 else 'NULL'
        validator_id = self.sync_dialog.validator.currentData()[0] if self.sync_dialog.validator.currentIndex() != 0 else ''
        validator_firstname = self.sync_dialog.validator.currentData()[2] if self.sync_dialog.validator.currentIndex() != 0 else ''
        validator_lastname = self.sync_dialog.validator.currentData()[1] if self.sync_dialog.validator.currentIndex() != 0 else ''
        validation_comment = self.sync_dialog.validation_comment.toPlainText() if self.sync_dialog.validation_comment.toPlainText() else ''
        self.sqlite_cur.execute(f"""UPDATE synchro SET import_name = '{import_name.replace("'", "''")}', 
        import_description = '{import_description.replace("'", "''")}', 
        contact_id = '{contact_id}', contact_name = '{contact_lastname.upper()} {contact_firstname.title()}',
        occ_hab_validation = {occ_hab_validation}, validator_id = '{validator_id}',
        validator_name = '{validator_lastname.upper()} {validator_firstname.title()}',
        validation_comment = '{validation_comment.replace("'", "''")}';""")
        self.sqlite_conn.commit()

    def create_import_directory(self) -> bool:
        """Create the directory on the server for saving csv files."""
        if not self.common_csv_dir.exists():
            self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                self.translator.tr('Synchronization has not been successfull'))
            self.iface.messageBar().pushWarning(self.translator.tr('Form'),
                                                self.translator.tr('Servor path does not exist'))
            return False
        if not self.sync_dialog.import_name.text():
            self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                self.translator.tr('Synchronization has not been successfull'))
            self.iface.messageBar().pushWarning(self.translator.tr('Form'),
                                                self.translator.tr('Empty values !'))
            return False
        dir_name_prefix = str(datetime.now()).replace(' ', '_').replace(':', '-').replace('.', '-')
        dir_name = re.sub(r'[^a-zA-Z0-9\s]+', '', strip_accents(self.sync_dialog.import_name.text())).lower().replace(' ', '_')
        self.csv_dir = self.common_csv_dir / '-'.join([dir_name_prefix, dir_name])
        try:
            self.csv_dir.mkdir()
        except FileExistsError:
            self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                self.translator.tr('Synchronization has not been successfull'))
            self.iface.messageBar().pushWarning(self.translator.tr('Error'),
                                                self.translator.tr('This directory name already exists on the servor'))
            return False
        return True

    def remove_empty_surveys(self):
        for survey in self.empty_surveys:
            if survey['geom_id'] is not None:
                geom_table = survey['geom_id'].split(' ')[0]
                geom_id = survey['geom_id'].split(' ')[1]
                self.synchro_cur.execute(f"DELETE FROM {geom_table} WHERE {geom_id}")
            self.synchro_cur.execute(f"DELETE FROM releves WHERE id = {survey['id']}")

    def remove_surveys_without_geom(self):
        for survey in self.surveys_without_geom:
            observations = json.loads(survey['observations'])
            observations = [obs for obs in observations if obs is not None]
            if len(observations) > 0:
                self.synchro_cur.execute(f"""DELETE FROM observations 
                WHERE obs_id IN {tuple(observations) if len(observations) > 1 else f'({repr(observations[0])})'}""")
            obs_habitats = json.loads(survey['obs_habitats'])
            obs_habitats = [obs_hab for obs_hab in obs_habitats if obs_hab is not None]
            if len(obs_habitats) > 0:
                for obs_hab in obs_habitats:
                    if f"obs_hab_id={obs_hab} SQL" in survey:
                        self.synchro_cur.execute(survey[f"obs_hab_id={obs_hab} SQL"])
                    else:
                        self.synchro_cur.execute(f"DELETE FROM obs_habitats WHERE obs_hab_id = {obs_hab}")
                        self.synchro_cur.execute(f"DELETE FROM liens_obs_habitat WHERE id_obs_hab_1 = {obs_hab} OR id_obs_hab_2 = {obs_hab}")
            self.synchro_cur.execute(f"DELETE FROM releves WHERE id = {survey['id']}")

    @staticmethod
    def get_fieldnames(dictionary):
        """Get keys names from a dict."""
        return [key for key in dictionary.keys()]

    def get_lobelia_value(self, key, list_name):
        # None key
        if key is None or key == '':
            return ''
        # EXCEPTION
        if v.parse(self.version) <= v.parse('1.1.5') and list_name == 'COEF_AD' and key == 9:
            print('EXCEPTION')
            return '(+)'
        # Version 1.3 and later
        if v.parse(self.version) >= v.parse('1.3'):
            value = self.sqlite_cur.execute(
                f"SELECT import_value FROM listes_valeurs WHERE key={str(key)} AND list_name = '{list_name}'").fetchone()
            if value is None:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('Matching error')
                error = self.translator.tr('Key {0} has not been found in {1} SQLite table for list {2}').format(
                    str(key), 'listes_valeurs', list_name)
                self.iface.messageBar().pushWarning(error_type, error)
                self.logs_to_save.append({'log_type': error_type, 'log': error})
                raise ValueError(error)
            return value[0]
        # Version 1.2 and early
        if v.parse(self.version) <= v.parse('1.2'):
            value = self.sqlite_cur.execute(
                f"SELECT lobelia_value FROM listes_valeurs_lobelia WHERE key={str(key)} AND list_name = '{list_name}'").fetchone()
            if value is None:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('Matching error')
                error = self.translator.tr('Key {0} has not been found in {1} SQLite table for list {2}').format(
                    str(key), 'listes_valeurs_lobelia', list_name)
                self.iface.messageBar().pushWarning(error_type, error)
                self.logs_to_save.append({'log_type': error_type, 'log': error})
                raise ValueError(error)
            return value[0]

    def build_source_csv(self) -> bool:
        # Check form values
        if not self.sync_dialog.import_name.text() or not self.sync_dialog.import_description.toPlainText() \
                or self.sync_dialog.contact.currentIndex() == 0 or self.sync_dialog.occ_hab_validation.currentIndex() == 0 \
                or (self.sync_dialog.occ_hab_validation.currentData() == 1 and self.sync_dialog.validator.currentIndex() == 0):
            self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                self.translator.tr('Synchronization has not been successfull'))
            self.iface.messageBar().pushWarning(self.translator.tr('Form'),
                                                self.translator.tr('Empty values !'))
            return False
        # Save data
        rows = [self.get_formatted_source_dict()]
        # Write csv
        with open(str(self.csv_dir / SOURCE_CSV_NAME), 'w', encoding='utf-8', newline='') as output_file:
            fieldnames = self.get_fieldnames(rows[0])
            writer = csv.DictWriter(output_file, fieldnames=fieldnames, delimiter=';')
            writer.writeheader()
            writer.writerows(rows)
        print('source_import.csv OK')
        return True

    def get_formatted_source_dict(self):
        return {
            'id_interne': 1,
            'libelle': self.sync_dialog.import_name.text(),
            'description': self.sync_dialog.import_description.toPlainText(),
            'date_import': date.today(),
            'referent': self.contact[0]
        }

    def build_observers_csv(self) -> bool:
        # Get SQLite obs_id of observers of interest
        obs_id_list = [row[0] for row in self.sqlite_cur.execute('SELECT DISTINCT obs_id FROM liens_releve_observateur')]
        if self.contact[0] not in obs_id_list:
            obs_id_list.append(self.contact[0])
        # Get SQLite data of observers of interest
        observers_list = self.sqlite_cur.execute(f"""SELECT obs_id, obs_nom, obs_prenom, obs_email FROM observateurs
        WHERE obs_id IN {tuple(obs_id_list) if len(obs_id_list) > 1 else f'({repr(obs_id_list[0])})'}""").fetchall()
        # For each observer...
        rows = []
        for sqlite_obs in observers_list:
            # Get obs_id and other data in Lobelia DB
            if v.parse(self.version) >= v.parse('1.3'):
                sql = f"""SELECT obs_id, obs_nom, obs_prenom, obs_date_naissance, obs_civilite, obs_particule, 
                obs_remarque, obs_adresse, obs_telperso, obs_mobile, obs_initiales, obs_plateforme_id, obs_date_deces, 
                obs_tag, CASE
                    WHEN a_controler THEN 1::varchar
                    WHEN NOT a_controler THEN 0::varchar
                    ELSE a_controler::varchar
                END AS a_controler
                FROM cbn_v.v_observateurs /*public.observateurs o*/
                WHERE obs_id = {str(sqlite_obs["obs_id"])}"""
            else:
                sql = f"""SELECT o.obs_id, obs_nom, obs_prenom, obs_date_naissance, obs_civilite, obs_particule, 
                obs_remarque, obs_adresse, obs_telperso, obs_mobile, obs_initiales, obs_plateforme_id, obs_date_deces, 
                obs_tag, CASE
                    WHEN a_controler THEN 1::varchar
                    WHEN NOT a_controler THEN 0::varchar
                    ELSE a_controler::varchar
                END AS a_controler
                FROM cbn_v.v_user u --public.user u
                LEFT JOIN cbn_v.v_observateurs o /*public.observateurs o*/ON o.obs_id = u.obs_id
                WHERE email = '{sqlite_obs["obs_email"]}' OR email like CONCAT('%', LOWER('{strip_accents(sqlite_obs["obs_nom"])}'), '@mnhn.fr')"""
            self.pg_cur.execute(sql)
            lobelia_obs = self.pg_cur.fetchall()
            # Check if result returns more than 1 observer
            if len(lobelia_obs) > 1:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('Matching error')
                error = self.translator.tr('L4Q observer {0} has been found more than once in the Lobelia DB').format(
                    f"({str(sqlite_obs['obs_id'])}) {sqlite_obs['obs_nom']} {sqlite_obs['obs_prenom']}")
                self.iface.messageBar().pushWarning(error_type, error)
                self.logs_to_save.append({'log_type': error_type, 'log': error})
                return False
            # Check if result is empty
            if len(lobelia_obs) == 0:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('Matching error')
                error = self.translator.tr('L4Q observer {0} has not been found in the Lobelia DB').format(
                    f"({str(sqlite_obs['obs_id'])}) {sqlite_obs['obs_nom']} {sqlite_obs['obs_prenom']}")
                self.iface.messageBar().pushWarning(error_type, error)
                self.logs_to_save.append({'log_type': error_type, 'log': error})
                return False
                # ### Add extra observers
                # sql = f"""SELECT o.obs_id, obs_nom, obs_prenom, obs_date_naissance, obs_civilite, obs_particule,
                #         obs_remarque, obs_adresse, obs_telperso, obs_mobile, obs_initiales, obs_plateforme_id, obs_date_deces,
                #         obs_tag, a_controler
                #         FROM cbn_v.v_observateurs o --public.observateurs o
                #         WHERE obs_nom = '{sqlite_obs["obs_nom"]}' AND obs_prenom = '{sqlite_obs["obs_prenom"]}'
                #         ORDER BY obs_id"""
                # self.pg_cur.execute(sql)
                # lobelia_obs = self.pg_cur.fetchall()
                # print(f"Observateur récupéré pour {sqlite_obs['obs_nom']} {sqlite_obs['obs_prenom']}")
                # if len(lobelia_obs) == 0:
                #     self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                #                                         self.translator.tr('Synchronization has not been successfull'))
                #     error_type = self.translator.tr('Matching error')
                #     error = self.translator.tr('L4Q observer {0} has not been found in the Lobelia DB').format(
                #         f"({str(sqlite_obs['obs_id'])}) {sqlite_obs['obs_nom']} {sqlite_obs['obs_prenom']}")
                #     self.iface.messageBar().pushWarning(error_type, error)
                #     # self.logs_to_save.append({'log_type': error_type, 'log': error})
                #     return False
            # Save data
            rows.append(self.get_formatted_observer_dict(sqlite_obs, lobelia_obs[0]))
        # Write csv
        with open(str(self.csv_dir / OBSERVERS_CSV_NAME), 'w', encoding='utf-8', newline='') as output_file:
            fieldnames = self.get_fieldnames(rows[0])
            writer = csv.DictWriter(output_file, fieldnames=fieldnames, delimiter=';')
            writer.writeheader()
            writer.writerows(rows)
        print('observateurs.csv OK')
        return True

    @staticmethod
    def get_formatted_observer_dict(sqlite_obs, lobelia_obs):
        return {
            'obs_id_interne': sqlite_obs['obs_id'],
            'obs_id': lobelia_obs['obs_id'],
            'obs_nom': lobelia_obs['obs_nom'],
            'obs_prenom': lobelia_obs['obs_prenom'],
            'obs_date_naissance': lobelia_obs['obs_date_naissance'],
            'obs_civilite': lobelia_obs['obs_civilite'],
            'obs_particule': lobelia_obs['obs_particule'],
            'obs_remarque': lobelia_obs['obs_remarque'],
            'obs_adresse': lobelia_obs['obs_adresse'],
            'obs_telperso': lobelia_obs['obs_telperso'],
            'obs_mobile': lobelia_obs['obs_mobile'],
            'obs_initiales': lobelia_obs['obs_initiales'],
            'obs_plateforme_id': lobelia_obs['obs_plateforme_id'],
            'obs_date_deces': lobelia_obs['obs_date_deces'],
            'obs_tag': lobelia_obs['obs_tag'],
            'obs_org_id_interne': [1],
            'a_controler': lobelia_obs['a_controler']
        }

    def build_organizations_csv(self) -> bool:
        # Get unique organization org_id and other data in Lobelia DB
        sql = f"""SELECT org_id, org_nom, org_nom_valide, org_text_id, org_logo, org_detail, org_uuid,
        org_plateforme_id, org_adresse, org_tel, org_email, org_tag
        FROM cbn_v.v_organismes --public.organismes
        WHERE org_nom = 'Conservatoire botanique national du Bassin Parisien'"""
        self.pg_cur.execute(sql)
        lobelia_org = self.pg_cur.fetchall()
        # Check if result is empty
        if len(lobelia_org) == 0:
            self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                self.translator.tr('Synchronization has not been successfull'))
            error_type = self.translator.tr('Matching error')
            error = self.translator.tr('L4Q organization "{0}" has not been found in the Lobelia DB').format('CBNBP')
            self.iface.messageBar().pushWarning(error_type, error)
            self.logs_to_save.append({'log_type': error_type, 'log': error})
            return False
        # Save data
        rows = [self.get_formatted_organization_dict(lobelia_org[0])]
        # Write csv
        with open(str(self.csv_dir / ORGANIZATIONS_CSV_NAME), 'w', encoding='utf-8', newline='') as output_file:
            fieldnames = self.get_fieldnames(rows[0])
            writer = csv.DictWriter(output_file, fieldnames=fieldnames, delimiter=';')
            writer.writeheader()
            writer.writerows(rows)
        print('organismes.csv OK')
        return True

    @staticmethod
    def get_formatted_organization_dict(lobelia_org):
        return {
            'org_id_interne': 1,
            'org_id': lobelia_org['org_id'],
            'org_nom': lobelia_org['org_nom'],
            'org_nom_valide': lobelia_org['org_nom_valide'],
            'org_text_id': lobelia_org['org_text_id'],
            'org_logo': lobelia_org['org_logo'],
            'org_detail': lobelia_org['org_detail'],
            'org_uuid': lobelia_org['org_uuid'],
            'org_plateforme_id': lobelia_org['org_plateforme_id'],
            'org_adresse': lobelia_org['org_adresse'],
            'org_tel': lobelia_org['org_tel'],
            'org_email': lobelia_org['org_email'],
            'org_tag': lobelia_org['org_tag']
        }

    def build_datasets_csv(self) -> bool:
        # Get SQLite data of datasets of interest
        datasets_list = self.sqlite_cur.execute(f"""SELECT id, uuid, libelle_court FROM jdd
        WHERE uuid IN {tuple(self.jdd_uuid_list) if len(self.jdd_uuid_list) > 1 else f'({repr(self.jdd_uuid_list[0])})'}""").fetchall()
        # For each dataset...
        rows = []
        for sqlite_dataset in datasets_list:
            # Get id and other data in Lobelia DB
            sql = f"""SELECT jdd.libelle, libelle_court, jdd.description, identifiant_jdd, date_creation, date_revision, 
            tdv.libelle AS type_donnees, mot_cle, ojv.libelle AS objectif_jdd, emprise_geographique, jdd.id, 
            cadre_acquisition_id, date_debut, date_fin,
            CASE
                WHEN valide THEN 1::varchar
                WHEN NOT valide THEN 0::varchar
                ELSE valide::varchar
            END AS valide,
            CASE
                WHEN rapportage_national THEN 1::varchar
                WHEN NOT rapportage_national THEN 0::varchar
                ELSE rapportage_national::varchar
            END AS rapportage_national,
            CASE
                WHEN publier THEN 1::varchar
                WHEN NOT publier THEN 0::varchar
                ELSE publier::varchar
            END AS publier, code_programme, 
            CASE
                WHEN restriction_obv THEN 1::varchar
                WHEN NOT restriction_obv THEN 0::varchar
                ELSE restriction_obv::varchar
            END AS restriction_obv, rs.libelle AS restriction_sinp, editeur, 
            CASE
                WHEN domaine_marin THEN 1::varchar
                WHEN NOT domaine_marin THEN 0::varchar
                ELSE domaine_marin::varchar
            END AS domaine_marin, 
            CASE
                WHEN domaine_terrestre THEN 1::varchar
                WHEN NOT domaine_terrestre THEN 0::varchar
                ELSE domaine_terrestre::varchar
            END AS domaine_terrestre, tag, /*pti.protocole_type_id, */tvi.territoire_value_id, mr.methode_recueil
            FROM metadonnees.jeu_de_donnees jdd 
            LEFT JOIN metadonnees.type_donnees_value tdv ON tdv.id = jdd.type_donnees 
            LEFT JOIN metadonnees.objectif_jdd_value ojv ON ojv.id = jdd.objectif_jdd 
            LEFT JOIN metadonnees.restriction_sinp rs ON rs.id = jdd.restriction_sinp
            --LEFT JOIN (
            --    SELECT jddpt.jdd_id, JSONB_AGG(DISTINCT pt.libelle) AS protocole_type_id
            --    FROM metadonnees.jeu_de_donnees_protocole_type jddpt
            --    LEFT JOIN metadonnees.protocole_type pt ON pt.protocole_type_id = jddpt.protocole_type_id
            --    GROUP BY jddpt.jdd_id
            --) pti ON pti.jdd_id = jdd.id
            LEFT JOIN (
                SELECT jddtv.jdd_id, JSONB_AGG(DISTINCT tv.libelle) AS territoire_value_id
                FROM metadonnees.jeu_de_donnees_territoire_value jddtv
                LEFT JOIN metadonnees.territoire_value tv ON tv.id = jddtv.territoire_value_id 
                GROUP BY jddtv.jdd_id
            ) tvi ON tvi.jdd_id = jdd.id
            LEFT JOIN (
                SELECT jddmrv.jdd_id, JSONB_AGG(DISTINCT mrv.libelle) AS methode_recueil
                FROM metadonnees.jeu_de_donnees_methode_recueil_value jddmrv
                LEFT JOIN metadonnees.methode_recueil_value mrv ON mrv.id = jddmrv.methode_recueil_value_id
                GROUP BY jddmrv.jdd_id
            ) mr ON mr.jdd_id = jdd.id
            WHERE identifiant_jdd = '{sqlite_dataset["uuid"]}'
            GROUP BY tdv.libelle, ojv.libelle, jdd.id, rs.libelle, /*pti.protocole_type_id, */tvi.territoire_value_id,
            mr.methode_recueil"""
            self.pg_cur.execute(sql)
            lobelia_dataset = self.pg_cur.fetchall()
            # Check if result is empty
            if len(lobelia_dataset) == 0:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('Matching error')
                error = self.translator.tr('L4Q dataset "{0}" has not been found in the Lobelia DB').format(sqlite_dataset['libelle_court'])
                self.iface.messageBar().pushWarning(error_type, error)
                self.logs_to_save.append({'log_type': error_type, 'log': error})
                return False
            # Check if result returns more than 1 dataset
            if len(lobelia_dataset) > 1:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('Matching error')
                error = self.translator.tr('L4Q dataset "{0}" has been found more than once in the Lobelia DB').format(sqlite_dataset['libelle_court'])
                self.iface.messageBar().pushWarning(error_type, error)
                self.logs_to_save.append({'log_type': error_type, 'log': error})
                return False
            # Save data
            rows.append(self.get_formatted_dataset_dict(sqlite_dataset, lobelia_dataset[0]))
        # Write csv
        with open(str(self.csv_dir / DATASETS_CSV_NAME), 'w', encoding='utf-8', newline='') as output_file:
            # Write header and rows
            fieldnames = self.get_fieldnames(rows[0])
            writer = csv.DictWriter(output_file, fieldnames=fieldnames, delimiter=';')
            writer.writeheader()
            writer.writerows(rows)
        print('jeu_de_donnees.csv OK')
        return True

    @staticmethod
    def get_formatted_dataset_dict(sqlite_dataset, lobelia_dataset):
        return {
            'id_interne': sqlite_dataset['id'],
            'libelle': lobelia_dataset['libelle'],
            'libelle_court': lobelia_dataset['libelle_court'],
            'description': lobelia_dataset['description'],
            'date_creation': lobelia_dataset['date_creation'],
            'date_revision': lobelia_dataset['date_revision'],
            'type_donnees': lobelia_dataset['type_donnees'],
            'identifiant_jdd': lobelia_dataset['identifiant_jdd'],
            'mot_cle': lobelia_dataset['mot_cle'],
            'objectif_jdd': lobelia_dataset['objectif_jdd'],
            'emprise_geographique': lobelia_dataset['emprise_geographique'],
            'id': lobelia_dataset['id'],
            'cadre_acquisition_id_interne': lobelia_dataset['cadre_acquisition_id'],
            'date_debut': lobelia_dataset['date_debut'],
            'date_fin': lobelia_dataset['date_fin'],
            'valide': lobelia_dataset['valide'],
            'rapportage_national': lobelia_dataset['rapportage_national'],
            'publier': lobelia_dataset['publier'],
            'code_programme': lobelia_dataset['code_programme'],
            'restriction_obv': lobelia_dataset['restriction_obv'],
            'restriction_sinp': lobelia_dataset['restriction_sinp'],
            'editeur': lobelia_dataset['editeur'],
            'domaine_marin': lobelia_dataset['domaine_marin'],
            'domaine_terrestre': lobelia_dataset['domaine_terrestre'],
            'tag': lobelia_dataset['tag'],
            # 'protocole_type_id': json.dumps(lobelia_dataset['protocole_type_id'], ensure_ascii=False) if lobelia_dataset['protocole_type_id'] else lobelia_dataset['protocole_type_id'],
            'territoire_value_id': json.dumps(lobelia_dataset['territoire_value_id'], ensure_ascii=False) if lobelia_dataset['territoire_value_id'] else lobelia_dataset['territoire_value_id'],
            'acteurs_type': None,
            'methode_recueil': json.dumps(lobelia_dataset['methode_recueil'], ensure_ascii=False) if lobelia_dataset['methode_recueil'] else lobelia_dataset['methode_recueil']
        }

    def build_frameworks_csv(self) -> bool:
        # Get frameworks id and other data in Lobelia DB
        sql = f"""SELECT ca.libelle, vsv.libelle AS volet_sinp, ca.description, identifiant_cadre, 
        ntv.libelle AS niveau_territorial, precision_geographique, ca.mot_cle, tfv.libelle AS type_financement, 
        date_creation_mtd, date_maj_mtd, description_cible, 
        CASE
            WHEN est_meta_cadre THEN 1::varchar
            WHEN NOT est_meta_cadre THEN 0::varchar
            ELSE est_meta_cadre::varchar
        END AS est_meta_cadre, date_lancement, date_cloture,
        CASE
            WHEN ca.publier THEN 1::varchar
            WHEN NOT ca.publier THEN 0::varchar
            ELSE ca.publier::varchar
        END AS publier, ca.id, id_tps, ca.tag, gti.groupe_taxonomique, ovi.objectif_value, tvi.territoire_value_id
        FROM metadonnees.cadre_acquisition ca
        LEFT JOIN metadonnees.jeu_de_donnees jdd ON jdd.cadre_acquisition_id = ca.id 
        LEFT JOIN metadonnees.volet_sinp_value vsv ON vsv.id = ca.volet_sinp 
        LEFT JOIN metadonnees.niveau_territorial_value ntv ON ntv.id = ca.niveau_territorial 
        LEFT JOIN metadonnees.type_financement_value tfv ON tfv.id = ca.type_financement
        LEFT JOIN (
            SELECT cagt.cadre_acquisition_id, JSONB_AGG(DISTINCT gt.libelle) AS groupe_taxonomique
            FROM metadonnees.cadre_acquisition_groupe_taxonomique cagt
            LEFT JOIN metadonnees.groupe_taxonomique gt ON gt.id = cagt.groupe_taxonomique_id 
            GROUP BY cagt.cadre_acquisition_id
        ) gti ON gti.cadre_acquisition_id = ca.id
        LEFT JOIN (
            SELECT caov.cadre_acquisition_id, JSONB_AGG(DISTINCT ov.libelle) AS objectif_value
            FROM metadonnees.cadre_acquisition_objectifs_value caov
            LEFT JOIN metadonnees.objectifs_value ov ON ov.id = caov.objectif_value_id 
            GROUP BY caov.cadre_acquisition_id
        ) ovi ON ovi.cadre_acquisition_id = ca.id
        LEFT JOIN (
            SELECT catv.cadre_acquisition_id, JSONB_AGG(DISTINCT tv.libelle) AS territoire_value_id
            FROM metadonnees.cadre_acquisition_territoire_value catv
            LEFT JOIN metadonnees.territoire_value tv ON tv.id = catv.territoire_value_id
            GROUP BY catv.cadre_acquisition_id
        ) tvi ON tvi.cadre_acquisition_id = ca.id
        WHERE jdd.identifiant_jdd IN {tuple(self.jdd_uuid_list) if len(self.jdd_uuid_list) > 1 else f'({repr(self.jdd_uuid_list[0])})'}
        GROUP BY vsv.libelle, ntv.libelle, tfv.libelle, ca.id, gti.groupe_taxonomique, ovi.objectif_value,
        tvi.territoire_value_id"""
        self.pg_cur.execute(sql)
        lobelia_frameworks = self.pg_cur.fetchall()
        # Check if result is empty
        if len(lobelia_frameworks) == 0:
            self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                self.translator.tr('Synchronization has not been successfull'))
            error_type = self.translator.tr('Matching error')
            error = self.translator.tr('L4Q framework has not been found in the Lobelia DB')
            self.iface.messageBar().pushWarning(error_type, error)
            self.logs_to_save.append({'log_type': error_type, 'log': error})
            return False
        # For each framework...
        rows = []
        for framework in lobelia_frameworks:
            # Save data
            rows.append(self.get_formatted_framework_dict(framework))
        # Write csv
        with open(str(self.csv_dir / FRAMEWORKS_CSV_NAME), 'w', encoding='utf-8', newline='') as output_file:
            fieldnames = self.get_fieldnames(rows[0])
            writer = csv.DictWriter(output_file, fieldnames=fieldnames, delimiter=';')
            writer.writeheader()
            writer.writerows(rows)
        print('cadre_acquisition.csv OK')
        return True

    def get_formatted_framework_dict(self, lobelia_framework):
        return {
            'id_interne': lobelia_framework['id'],
            'libelle': lobelia_framework['libelle'],
            'volet_sinp': lobelia_framework['volet_sinp'],
            'description': lobelia_framework['description'],
            'identifiant_cadre': lobelia_framework['identifiant_cadre'],
            'niveau_territorial': lobelia_framework['niveau_territorial'],
            'precision_geographique': lobelia_framework['precision_geographique'],
            'mot_cle': lobelia_framework['mot_cle'],
            'type_financement': lobelia_framework['type_financement'],
            'date_creation_mtd': lobelia_framework['date_creation_mtd'],
            'date_maj_mtd': lobelia_framework['date_maj_mtd'],
            'description_cible': lobelia_framework['description_cible'],
            'est_meta_cadre': lobelia_framework['est_meta_cadre'],
            'date_lancement': lobelia_framework['date_lancement'],
            'date_cloture': lobelia_framework['date_cloture'],
            'publier': lobelia_framework['publier'],
            'id': lobelia_framework['id'],
            'id_interne_meta_cadre_parent': None,
            'editeur_id_interne': self.contact[0],
            'id_tps': lobelia_framework['id_tps'],
            'tag': lobelia_framework['tag'],
            'acteurs_type': None,
            'groupe_taxonomique': json.dumps(lobelia_framework['groupe_taxonomique'], ensure_ascii=False) if lobelia_framework['groupe_taxonomique'] else lobelia_framework['groupe_taxonomique'],
            'objectif_value': json.dumps(lobelia_framework['objectif_value'], ensure_ascii=False) if lobelia_framework['objectif_value'] else lobelia_framework['objectif_value'],
            'territoire_value_id': json.dumps(lobelia_framework['territoire_value_id'], ensure_ascii=False) if lobelia_framework['territoire_value_id'] else lobelia_framework['territoire_value_id']
        }

    def build_surveys_csv(self) -> bool:
        # Get SQLite data of surveys
        surveys_list = self.synchro_cur.execute(f"""WITH prep_pedologies AS (
            SELECT id, JSON_OBJECT(
                'horizon_id', 0, 
                'texture_princ_id', rel_texture_id_ha_princ,
                'texture_sec_id', rel_texture_id_ha_sec,
                'texture_tert_id', rel_texture_id_ha_tert,
                'react_hcl_mat_fine_id', rel_reaction_hcl_mat_fine_ha,
                'react_hcl_elem_gros_id', rel_reaction_hcl_elem_gros_ha,
                'elem_gros_dom_id', rel_elem_grossiers_ha,
                'pct_elem_gros', CASE WHEN rel_pct_elem_grossiers_ha IS NOT NULL THEN rel_pct_elem_grossiers_ha ELSE '' END) AS pedo
            FROM releves
            WHERE (rel_texture_id_ha_princ IS NOT NULL AND rel_texture_id_ha_princ NOT IN (0, '')) OR (rel_texture_id_ha_sec IS NOT NULL AND rel_texture_id_ha_sec NOT IN (0, '')) OR (rel_texture_id_ha_tert IS NOT NULL AND rel_texture_id_ha_tert NOT IN (0, '')) OR (rel_reaction_hcl_mat_fine_ha IS NOT NULL AND rel_reaction_hcl_mat_fine_ha NOT IN (0, '')) OR (rel_reaction_hcl_elem_gros_ha IS NOT NULL AND rel_reaction_hcl_elem_gros_ha NOT IN (0, '')) OR (rel_elem_grossiers_ha IS NOT NULL AND rel_elem_grossiers_ha NOT IN (0, '')) OR (rel_pct_elem_grossiers_ha IS NOT NULL AND rel_pct_elem_grossiers_ha NOT IN (0, ''))
            UNION
            SELECT id, JSON_OBJECT(
                'horizon_id', 1, 
                'texture_princ_id', rel_texture_id_hb_princ,
                'texture_sec_id', rel_texture_id_hb_sec,
                'texture_tert_id', rel_texture_id_hb_tert,
                'react_hcl_mat_fine_id', rel_reaction_hcl_mat_fine_hb,
                'react_hcl_elem_gros_id', rel_reaction_hcl_elem_gros_hb,
                'elem_gros_dom_id', rel_elem_grossiers_hb,
                'pct_elem_gros', CASE WHEN rel_pct_elem_grossiers_hb IS NOT NULL THEN rel_pct_elem_grossiers_hb ELSE '' END) AS pedo
            FROM releves
            WHERE (rel_texture_id_hb_princ IS NOT NULL AND rel_texture_id_hb_princ NOT IN (0, '')) OR (rel_texture_id_hb_sec IS NOT NULL AND rel_texture_id_hb_sec NOT IN (0, '')) OR (rel_texture_id_hb_tert IS NOT NULL AND rel_texture_id_hb_tert NOT IN (0, '')) OR (rel_reaction_hcl_mat_fine_hb IS NOT NULL AND rel_reaction_hcl_mat_fine_hb NOT IN (0, '')) OR (rel_reaction_hcl_elem_gros_hb IS NOT NULL AND rel_reaction_hcl_elem_gros_hb NOT IN (0, '')) OR (rel_elem_grossiers_hb IS NOT NULL AND rel_elem_grossiers_hb NOT IN (0, '')) OR (rel_pct_elem_grossiers_hb IS NOT NULL AND rel_pct_elem_grossiers_hb NOT IN (0, ''))
        )
        SELECT r.id, 
        CASE
            WHEN rel_point_uuid IS NOT NULL AND rel_point_uuid != '' THEN rel_point_uuid
            WHEN rel_ligne_uuid IS NOT NULL AND rel_ligne_uuid != '' THEN rel_ligne_uuid
            WHEN rel_polygon_uuid IS NOT NULL AND rel_polygon_uuid != '' THEN rel_polygon_uuid
        END AS geom_uuid, {'rel_date,' if v.parse(self.version) >= v.parse('1.4') else ''} date_obs,
        CASE
            WHEN date_obs = date_obs_max THEN ''
            ELSE date_obs
        END AS rel_date_sup,
        CASE
            WHEN rel_point_uuid IS NOT NULL AND rel_point_uuid != '' AND sp.uuid IS NOT NULL THEN 'station_point id=' || sp.id
            WHEN rel_ligne_uuid IS NOT NULL AND rel_ligne_uuid != '' AND sl.uuid IS NOT NULL THEN 'station_ligne id=' || sl.id
            WHEN rel_polygon_uuid IS NOT NULL AND rel_polygon_uuid != '' AND sf.uuid IS NOT NULL THEN 'station_surface id=' || sf.id
        END AS geom_id,
        CASE
            WHEN rel_point_uuid IS NOT NULL AND rel_point_uuid != '' THEN AsText(sp.geom)
            WHEN rel_ligne_uuid IS NOT NULL AND rel_ligne_uuid != '' THEN AsText(sl.geom)
            WHEN rel_polygon_uuid IS NOT NULL AND rel_polygon_uuid != '' THEN AsText(sf.geom)
        END AS the_geom,
        CASE
            WHEN rel_point_uuid IS NOT NULL AND rel_point_uuid != '' THEN {self.geom_layers_crs['station_point']}
            WHEN rel_ligne_uuid IS NOT NULL AND rel_ligne_uuid != '' THEN {self.geom_layers_crs['station_ligne']}
            WHEN rel_polygon_uuid IS NOT NULL AND rel_polygon_uuid != '' THEN {self.geom_layers_crs['station_surface']}
        END AS projection, rel_lieu_dit, rel_prec_loc, rel_type_rel_id AS rel_type_id, rel_protocole_id,
        jdd.id AS rel_jdd_id_interne, rel_technique_collecte_id, rel_source_extrapol, rel_code_pointage, rel_num,
        rel_qualite_id, rel_exhaustivite_id, rel_coherence_tempo_id,
        CASE
            WHEN rel_temps_passe >= 0 AND rel_temps_passe <= 5 THEN "De 0 à 5 minutes"
            WHEN rel_temps_passe > 5 AND rel_temps_passe <= 10 THEN "De 5 à 10 minutes"
            WHEN rel_temps_passe > 10 AND rel_temps_passe <= 15 THEN "De 10 à 15 minutes"
            WHEN rel_temps_passe > 15 AND rel_temps_passe <= 30 THEN "De 15 à 30 minutes"
            WHEN rel_temps_passe > 30 AND rel_temps_passe <= 45 THEN "De 30 à 45 minutes"
            WHEN rel_temps_passe > 45 AND rel_temps_passe <= 60 THEN "De 45 minutes à 1 heure"
            WHEN rel_temps_passe > 60 AND rel_temps_passe <= 120 THEN "De 1 à 2 heures"
            WHEN rel_temps_passe > 120 AND rel_temps_passe <= 180 THEN "De 2 à 3 heures"
            WHEN rel_temps_passe > 180 AND rel_temps_passe <= 240 THEN "De 3 à 4 heures"
            WHEN rel_temps_passe > 240 AND rel_temps_passe <= 360 THEN "De 4 à 6 heures"
            WHEN rel_temps_passe > 360 THEN "1 journée"
        END AS rel_temps_passe_id, commentaires, rel_altitude, rel_pente_id, rel_exposition_id, rel_freq_immersion_id,
        rel_taille_cours_eau_id, rel_vitesse_eau_id, comm_infos_stat, /*rel_forme_id*/rel_surface, rel_fragmente,
        rel_rec_total, rel_rec_litiere, rel_sol_nu, rel_pres_bois_mort_id, rel_type_humus_id, rel_tache_redox,
        rel_pres_tache_redox, rel_horizon_redox, rel_pres_horizon_redox, rel_horizon_reduct, rel_pres_horizon_reduct,
        rel_type_sol_id, {'rel_profondeur_sol, rel_prof_sol_sup,' if v.parse(self.version) >= v.parse('1.3') else ''}
        rel_roche_mere_id, rel_obs_syntaxon_id, rel_unite_vege_id, lro.observateurs, lri.fact_evol, pp.pedologies, psp.strates_phyto,
        CASE
            WHEN rel_topo_station_id IS NOT NULL AND rel_topo_station_id NOT IN (0, '') THEN JSON_GROUP_ARRAY(rel_topo_station_id)
            ELSE NULL
        END AS topo_station
        FROM releves r
        LEFT JOIN station_point sp ON sp.uuid = r.rel_point_uuid
        LEFT JOIN station_ligne sl ON sl.uuid = r.rel_ligne_uuid
        LEFT JOIN station_surface sf ON sf.uuid = r.rel_polygon_uuid
        LEFT JOIN jdd ON jdd.uuid = r.rel_jdd_uuid
        LEFT JOIN (
            SELECT rel_id, JSON_GROUP_ARRAY(JSON_OBJECT('obs_id_interne', lro.obs_id, 'org_id_interne', 1)) AS observateurs
            FROM liens_releve_observateur lro
            GROUP BY rel_id
        ) lro ON lro.rel_id = r.id
        LEFT JOIN (
            SELECT rel_id, JSON_GROUP_ARRAY(JSON_OBJECT(
                'fac_evol_id', facteur_id,
                'impact_fac_evol_id', impact_id,
                'etat_fact_evol_id', etat_id)) AS fact_evol
            FROM liens_releve_influence
            GROUP BY rel_id
        ) lri ON lri.rel_id = r.id
        LEFT JOIN (
            SELECT id, JSON_GROUP_ARRAY(JSON(pedo)) AS pedologies
            FROM prep_pedologies
            GROUP BY id
        ) pp ON pp.id = r.id
        LEFT JOIN (
            SELECT rel_id, JSON_GROUP_ARRAY(JSON_OBJECT(
                'recouvrement', CASE WHEN recouvrement IS NOT NULL AND recouvrement != 0 THEN recouvrement ELSE '' END,
                'h_min', CASE WHEN hauteur_min IS NOT NULL AND hauteur_min != 0 THEN hauteur_min ELSE '' END,
                'h_max', CASE WHEN hauteur_max IS NOT NULL AND hauteur_max != 0 THEN hauteur_max ELSE '' END,
                'h_moy', CASE WHEN hauteur_modale IS NOT NULL AND hauteur_modale != 0 THEN hauteur_modale ELSE '' END,
                'strate_id', strate_id)) AS strates_phyto
            FROM liens_releve_strate
            GROUP BY rel_id
        ) psp ON psp.rel_id = r.id
        GROUP BY r.id
        ORDER BY r.id""").fetchall()
        # Initialize progress bar
        progress_message_bar = self.iface.messageBar().createMessage('Synchronisation des relevés...')
        progress = QProgressBar()
        progress.setMaximum(len(surveys_list))
        progress.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)
        progress_message_bar.layout().addWidget(progress)
        self.iface.messageBar().pushWidget(progress_message_bar, Qgis.Info)
        # For each survey...
        rows = []
        for i, survey in enumerate(surveys_list):
            # Get municipalities and departments intersections in Lobelia DB
            sql_geom = f"""WITH survey_geom AS (
                SELECT CASE
                    WHEN ST_IsValidReason(ST_GeomFromText('{survey['the_geom']}', 2154)) != 'Valid Geometry' THEN ST_MakeValid(ST_GeomFromText('{survey['the_geom']}', 2154))
                    ELSE ST_GeomFromText('{survey['the_geom']}', 2154)
                END AS survey_geom
            )
            SELECT row_number() OVER() AS id, JSON_AGG(insee_comm) AS rel_insee_com, JSON_AGG(DISTINCT insee_dept) AS rel_dept_id
            FROM referentiel.communes_fr
            JOIN survey_geom ON ST_intersects(the_geom, survey_geom)"""
            self.pg_cur.execute(sql_geom)
            lobelia_municipalities = self.pg_cur.fetchall()
            # Get some properties from obs_habitats table
            obs_hab_properties = None
            if survey['rel_obs_syntaxon_id'] is not None:
                obs_hab_properties = self.synchro_cur.execute(f"""SELECT syntaxon_ombrage_id, syntaxon_salinite_id, 
                syntaxon_ph_sol_id, syntaxon_niv_trophie_id, syntaxon_hum_edaph_id, syntaxon_type_bio_id, 
                syntaxon_enneigement_id, syntaxon_thermie_id, comm_syntaxons_contact, comm_desc_gen,
                CASE
                    WHEN syntaxon_type_physio_id IS NOT NULL AND syntaxon_type_physio_id NOT IN (0, '') THEN JSON_GROUP_ARRAY(syntaxon_type_physio_id)
                    ELSE NULL
                END AS physionomies
                FROM obs_habitats
                WHERE obs_hab_id = {survey['rel_obs_syntaxon_id']}""").fetchone()
            # Check if result is empty
            if len(lobelia_municipalities) == 0:
                self.iface.messageBar().pushWarning(self.translator.tr('Synchronization'),
                                                    self.translator.tr('Synchronization has not been successfull'))
                error_type = self.translator.tr('Error')
                error = self.translator.tr('Survey id={0} could not have been intersected with any municipality of the Lobelia DB').format(f"{str(survey['id'])} ({survey['geom_id']})")
                self.iface.messageBar().pushWarning(error_type, error)
                self.logs_to_save.append({'log_type': error_type, 'log': error})
                return False
            # Save data
            rows.append(self.get_formatted_survey_dict(survey, lobelia_municipalities[0], obs_hab_properties))
            # Increment progress bar
            progress.setValue(i + 1)
        # Write csv
        with open(str(self.csv_dir / SURVEYS_CSV_NAME), 'w', encoding='utf-8', newline='') as output_file:
            fieldnames = self.get_fieldnames(rows[0])
            writer = csv.DictWriter(output_file, fieldnames=fieldnames, delimiter=';')
            writer.writeheader()
            writer.writerows(rows)
        self.iface.messageBar().clearWidgets()
        print('releves.csv OK')
        return True

    def get_formatted_fact_evol_item(self, fact_evol_item):
        fact_evol_item['fac_evol_id'] = self.get_lobelia_value(fact_evol_item['fac_evol_id'], 'FACTEUR_INFLUENCE_VALUES')
        fact_evol_item['impact_fac_evol_id'] = self.get_lobelia_value(fact_evol_item['impact_fac_evol_id'], 'FACTEUR_IMPACT_VALUES')
        fact_evol_item['etat_fact_evol_id'] = self.get_lobelia_value(fact_evol_item['etat_fact_evol_id'], 'FACTEUR_ETAT_VALUES')
        return fact_evol_item

    def get_formatted_pedologies_item(self, pedo_item):
        pedo_item['texture_princ_id'] = self.get_lobelia_value(pedo_item['texture_princ_id'], 'TEXTURE_SOL_VALUES')
        pedo_item['texture_sec_id'] = self.get_lobelia_value(pedo_item['texture_sec_id'], 'TEXTURE_SOL_VALUES')
        pedo_item['texture_tert_id'] = self.get_lobelia_value(pedo_item['texture_tert_id'], 'TEXTURE_SOL_VALUES')
        pedo_item['react_hcl_mat_fine_id'] = self.get_lobelia_value(pedo_item['react_hcl_mat_fine_id'], 'REACTION_HCL_VALUES')
        pedo_item['react_hcl_elem_gros_id'] = self.get_lobelia_value(pedo_item['react_hcl_elem_gros_id'], 'REACTION_HCL_VALUES')
        pedo_item['elem_gros_dom_id'] = self.get_lobelia_value(pedo_item['elem_gros_dom_id'], 'ELEMENTS_GROSSIERS_VALUES')
        return pedo_item

    def get_formatted_strates_phyto_item(self, strates_phyto_item, rec_sol_nu, survey_id):
        strates_phyto_item['strate_id'] = self.get_lobelia_value(strates_phyto_item['strate_id'], 'STRATES_VALUES')
        if strates_phyto_item['strate_id'] != 'Recouvrement sol nu':
            return strates_phyto_item
        else:
            log = f'La valeur "Recouvrement sol nu" est présente dans la table liens_releve_strate du relevé id={survey_id}'
            print(log)
            self.logs_to_save.append({'log_type': self.translator.tr('SQLite DB'), 'log': log})
            if strates_phyto_item['recouvrement'] is not None and strates_phyto_item['recouvrement'] != 0:
                rec_sol_nu.append(strates_phyto_item['recouvrement'])
            return None

    def get_formatted_survey_dict(self, survey, municipalities, obs_hab_properties=None):
        fact_evol = survey['fact_evol']
        if fact_evol:
            fact_evol = list(map(lambda item: self.get_formatted_fact_evol_item(item), json.loads(fact_evol)))
        pedologies = survey['pedologies']
        if pedologies:
            pedologies = list(map(lambda item: self.get_formatted_pedologies_item(item), json.loads(pedologies)))
        physionomies = obs_hab_properties['physionomies'] if obs_hab_properties else None
        if physionomies:
            physionomies = list(map(lambda item: self.get_lobelia_value(item, 'TYPE_PHYSIO_VALUES'), json.loads(physionomies)))
        strates_phyto = survey['strates_phyto']
        rec_sol_nu = []
        if strates_phyto:
            strates_phyto = list(map(lambda item: self.get_formatted_strates_phyto_item(item, rec_sol_nu, survey['id']), json.loads(strates_phyto)))
            strates_phyto = [item for item in strates_phyto if item is not None]
            if len(strates_phyto) == 0:
                strates_phyto = None
        rec_sol_nu = rec_sol_nu[0] if len(rec_sol_nu) > 0 else None
        if rec_sol_nu is not None and (survey['rel_sol_nu'] is None or survey['rel_sol_nu'] == ''):
            log = 'et la valeur de recouvrement associée a été sauvegardée dans le champ "rel_sol_nu"'
            print(log)
            self.logs_to_save.append({'log_type': self.translator.tr('SQLite DB'), 'log': log})
        topo_station = survey['topo_station']
        if topo_station:
            topo_station = list(map(lambda item: self.get_lobelia_value(item, 'TOPOGRAPHIE_VALUES'), json.loads(topo_station)))
        return {
            'id_interne': survey['id'],
            'rel_id': None,
            'rel_uuid': survey['geom_uuid'],
            'rel_id_origine': f"L4Q:{survey['id']}",
            'rel_date': survey['rel_date'] if v.parse(self.version) >= v.parse('1.4') else date.today(),
            'rel_date_obs': survey['date_obs'],
            'rel_date_inf': survey['date_obs'],
            'rel_date_sup': survey['rel_date_sup'],
            'rel_prec_date_id': None,
            'rel_date_rq': None,
            'the_geom': survey['the_geom'] if survey['the_geom'].startswith('MULTIPOINT') and survey['the_geom'].find(',') != -1
            else survey['the_geom'].replace('MULTIPOINT', 'POINT'),
            'projection': survey['projection'],
            'rel_coord_x': None,
            'rel_coord_y': None,
            'rel_tampon': None,
            'rel_lieu_dit': survey['rel_lieu_dit'],
            'rel_prec_loc': survey['rel_prec_loc'],
            'rel_t_localisation': None,
            'rel_localisation_incertaine': None,
            'rel_insee_com': json.dumps(municipalities['rel_insee_com']),
            'rel_dept_id': json.dumps(municipalities['rel_dept_id']),
            'rel_type_id': self.get_lobelia_value(survey['rel_type_id'], 'TYPE_REL_VALUES'),
            'rel_protocole': self.get_lobelia_value(survey['rel_protocole_id'], 'PROTOCOLE_VALUES'),
            'rel_type_rel_id': 'Aucun',
            'tag': 'cbnbp',
            'rel_saisi_obs_id_interne': self.contact[0],
            'rel_lastmodif_obs_id_interne': None,
            'rel_integration': 1,
            'rel_restriction': 0,
            'rel_jdd_id_interne': survey['rel_jdd_id_interne'],
            'rel_origine_id': 'Terrain',
            'rel_technique_collecte': self.get_lobelia_value(survey['rel_technique_collecte_id'], 'TECHNIQUE_COLLECTE_VALUES'),
            'rel_source_extrapol': survey['rel_source_extrapol'],
            'rel_code_pointage': survey['rel_code_pointage'],
            'rel_num': survey['rel_num'],
            'rel_qualite_id': self.get_lobelia_value(survey['rel_qualite_id'], 'QUALITE_RELEVE_VALUES'),
            'rel_exhaustivite': self.get_lobelia_value(survey['rel_exhaustivite_id'], 'EXHAUSTIVITE_VALUES'),
            'rel_coherence_tempo': self.get_lobelia_value(survey['rel_coherence_tempo_id'], 'COHERENCE_TEMPORELLE_VALUES'),
            'rel_temps_passe_id': survey['rel_temps_passe_id'],
            'rel_rq': survey['commentaires'],
            'rel_milieu': None,
            'rel_alt_min': None,
            'rel_alt_max': None,
            'rel_alt_moy': None,
            'rel_altitude': survey['rel_altitude'],
            'rel_pente_degre': None,
            'rel_pente_pct': None,
            'rel_pente_id': self.get_lobelia_value(survey['rel_pente_id'], 'PENTE_VALUES'),
            'rel_exposition_id': self.get_lobelia_value(survey['rel_exposition_id'], 'EXPOSITION_VALUES'),
            'rel_hydromorphie_id': None,
            'rel_freq_immersion_id': self.get_lobelia_value(survey['rel_freq_immersion_id'], 'FREQUENCE_IMMERSION_VALUES'),
            # 'rel_niveau_eau': None,
            'rel_taille_piece_eau': self.get_lobelia_value(survey['rel_taille_cours_eau_id'], 'TAILLE_COURS_EAU_VALUES'),
            'rel_vitesse_eau': self.get_lobelia_value(survey['rel_vitesse_eau_id'], 'VITESSE_EAU_VALUES'),
            'rel_rq_stationnelle': survey['comm_infos_stat'],
            'rel_forme': None,  # TODO /!\ FORME_RELEVE_VALUES intégré seulement à partir de v1.1.6 donc avant ça, rel_forme_id est inutilisable (TYPE_FORME_VALUES était la liste de valeurs de référence)
            'rel_surf': survey['rel_surface'],
            'rel_releve_frag': survey['rel_fragmente'],
            'rel_rec_tot_pc': survey['rel_rec_total'],
            'rel_litiere_pc': survey['rel_rec_litiere'],
            'rel_sol_nu_pc': survey['rel_sol_nu'] if survey['rel_sol_nu'] is not None and survey['rel_sol_nu'] != '' else rec_sol_nu,
            'rel_bois_mort': self.get_lobelia_value(survey['rel_pres_bois_mort_id'], 'BOIS_MORT_VALUES'),
            'rel_t_humus_id': self.get_lobelia_value(survey['rel_type_humus_id'], 'FORME_HUMUS_VALUES'),
            'rel_pres_hydro_taches_oxred': survey['rel_pres_tache_redox'],
            'rel_taches_oxred': survey['rel_tache_redox'],
            'rel_pres_hydro_hori_redox': survey['rel_pres_horizon_redox'],
            'rel_horizon_redox': survey['rel_horizon_redox'],
            'rel_pres_hydro_hori_reduct': survey['rel_pres_horizon_reduct'],
            'rel_horizon_reduct': survey['rel_horizon_reduct'],
            'rel_t_sol_id': self.get_lobelia_value(survey['rel_type_sol_id'], 'TYPE_SOL_VALUES'),
            'rel_profondeur_sol': survey['rel_profondeur_sol'] if v.parse(self.version) >= v.parse('1.3') else None,
            'rel_prof_sol_sup': survey['rel_prof_sol_sup'] if v.parse(self.version) >= v.parse('1.3') else None,
            'rel_roche_mere_id': self.get_lobelia_value(survey['rel_roche_mere_id'], 'ROCHE_MERE_VALUES'),
            'rel_ombrage_id': self.get_lobelia_value(obs_hab_properties['syntaxon_ombrage_id'], 'OMBRAGE_VALUES') if obs_hab_properties else None,
            'rel_salinite_id': self.get_lobelia_value(obs_hab_properties['syntaxon_salinite_id'], 'SALINITE_VALUES') if obs_hab_properties else None,
            'rel_ph_sol_id': self.get_lobelia_value(obs_hab_properties['syntaxon_ph_sol_id'], 'PH_SOL_VALUES') if obs_hab_properties else None,
            'rel_niv_trophie_id': self.get_lobelia_value(obs_hab_properties['syntaxon_niv_trophie_id'], 'NIV_TROPHIE_VALUES') if obs_hab_properties else None,
            'rel_humidite_id': self.get_lobelia_value(obs_hab_properties['syntaxon_hum_edaph_id'], 'HUMIDITE_VALUES') if obs_hab_properties else None,
            'rel_type_bio_id': self.get_lobelia_value(obs_hab_properties['syntaxon_type_bio_id'], 'TYPE_BIO_VALUES') if obs_hab_properties else None,
            'rel_enneigement': self.get_lobelia_value(obs_hab_properties['syntaxon_enneigement_id'], 'ENNEIGEMENT_VALUES') if obs_hab_properties else None,
            'rel_thermie': self.get_lobelia_value(obs_hab_properties['syntaxon_thermie_id'], 'THERMIE_VALUES') if obs_hab_properties else None,
            'rel_syntaxon_contact': obs_hab_properties['comm_syntaxons_contact'] if obs_hab_properties else None,
            'rel_unite_vege': self.get_lobelia_value(survey['rel_unite_vege_id'], 'UNITE_VEGE_VALUES'),
            'rel_ph_estime_id': None,
            'rel_type_culture': None,
            'rel_biblio_auteur_doc': None,
            'rel_biblio_date_doc': None,
            'rel_biblio_nom_doc': None,
            'rel_biblio_repere_doc': None,
            'rel_biblio_reference_doc': None,
            'rel_biblio_pmb_notice_id': None,
            'rel_typo_rq': obs_hab_properties['comm_desc_gen'] if obs_hab_properties else None,
            'code_eur': None,
            'code_eunis': None,
            'code_cahab': None,
            'code_cb': None,
            'observateurs': survey['observateurs'],
            'menaces': json.dumps(fact_evol, ensure_ascii=False) if fact_evol else fact_evol,
            'milieu_id': None,
            'pedologies': json.dumps(pedologies, ensure_ascii=False) if pedologies else pedologies,
            'physio_id': json.dumps(physionomies, ensure_ascii=False) if physionomies else physionomies,
            'strates_phyto': json.dumps(strates_phyto, ensure_ascii=False) if strates_phyto else strates_phyto,
            'topo_paysagere': None,
            'topo_station': json.dumps(topo_station, ensure_ascii=False) if topo_station else topo_station
        }

    def build_observations_csv(self) -> bool:
        # Get SQLite data of observations
        observations_list = self.synchro_cur.execute(f"""WITH prep_coeff_phyto AS (
            SELECT obs_id, JSON_OBJECT('strate_id', 'Strate arborée', 'ad_id', strate_arbo) AS strate
            FROM observations
            WHERE strate_arbo IS NOT NULL AND strate_arbo NOT IN (0, '')
            UNION
            SELECT obs_id, JSON_OBJECT('strate_id', 'Strate arbustive', 'ad_id', strate_arbu) AS strate
            FROM observations
            WHERE strate_arbu IS NOT NULL AND strate_arbu NOT IN (0, '')
            UNION
            SELECT obs_id, JSON_OBJECT('strate_id', 'Strate herbacée', 'ad_id', strate_herb) AS strate
            FROM observations
            WHERE strate_herb IS NOT NULL AND strate_herb NOT IN (0, '')
            UNION
            SELECT obs_id, JSON_OBJECT('strate_id', 'Strate bryolichenique (mousses, lichens et sphaignes)', 'ad_id', strate_musc) AS strate
            FROM observations
            WHERE strate_musc IS NOT NULL AND strate_musc NOT IN (0, '')
        )
        SELECT obs.obs_id, nom_cite, obs_rel_id, obs_rel_statut_determ_id, obs_rel_statut_spon_id, 
        cd_nom, obs_rel_classe_surfaces_id, obs_rel_classe_effectifs_id, obs_uuid, obs_rel_support_id,
        obs_rel_indice_aggreg_id, pcp.coeff_phyto,
        CASE
            WHEN obs_rel_statut_pheno_id IS NOT NULL AND obs_rel_statut_pheno_id NOT IN (0, '') THEN JSON_GROUP_ARRAY(JSON_OBJECT('stat_pheno_id', obs_rel_statut_pheno_id, 'taux', 100))
            ELSE NULL
        END AS phenologies
        FROM observations obs
        LEFT JOIN (
            SELECT obs_id, JSON_GROUP_ARRAY(JSON(strate)) AS coeff_phyto
            FROM prep_coeff_phyto
            GROUP BY obs_id
        ) pcp ON pcp.obs_id = obs.obs_id
        GROUP BY obs.obs_id
        ORDER BY obs.obs_id""").fetchall()
        # For each observation...
        rows = []
        for obs in observations_list:
            # Save data
            rows.append(self.get_formatted_observation_dict(obs))
        rows.append(self.get_formatted_observation_dict())  # Csv must be created even if L4Q project does not contain any observation
        # Write csv
        with open(str(self.csv_dir / OBSERVATIONS_CSV_NAME), 'w', encoding='utf-8', newline='') as output_file:
            fieldnames = self.get_fieldnames(rows[0])
            writer = csv.DictWriter(output_file, fieldnames=fieldnames, delimiter=';')
            writer.writeheader()
            rows = rows[:-1]  # Delete useless empty row
            writer.writerows(rows)
        print('observations.csv OK')
        return True

    def get_formatted_coeff_phyto_item(self, coeff_phyto_item):
        coeff_phyto_item['ad_id'] = self.get_lobelia_value(coeff_phyto_item['ad_id'], 'COEF_AD_VALUES')
        return coeff_phyto_item

    def get_formatted_pheno_item(self, pheno_item):
        if self.get_lobelia_value(pheno_item['stat_pheno_id'], 'STATUT_PHENOLOGIQUE_VALUES') != '':
            pheno_item['stat_pheno_id'] = self.get_lobelia_value(pheno_item['stat_pheno_id'], 'STATUT_PHENOLOGIQUE_VALUES')
            return pheno_item
        else:
            return None

    def get_formatted_observation_dict(self, observation=None):
        coeff_phyto = observation['coeff_phyto'] if observation else None
        if coeff_phyto:
            coeff_phyto = list(map(lambda item: self.get_formatted_coeff_phyto_item(item), json.loads(coeff_phyto)))
        phenologies = observation['phenologies'] if observation else None
        if phenologies:
            phenologies = list(map(lambda item: self.get_formatted_pheno_item(item), json.loads(phenologies)))
            phenologies = [item for item in phenologies if item is not None]
            if len(phenologies) == 0:
                phenologies = None
        return {
            'id_interne': observation['obs_id'] if observation else None,
            'obs_rel_id': None,
            'obs_rel_saisi_obs_id_interne': self.contact[0] if observation else None,
            'obs_rel_taxon_cite': observation['nom_cite'] if observation else None,
            'obs_rel_rq': None,
            'obs_rel_date': date.today() if observation else None,
            'obs_rel_rel_id_interne': observation['obs_rel_id'] if observation else None,
            'obs_rel_stat_det_id': self.get_lobelia_value(observation['obs_rel_statut_determ_id'], 'STATUT_DETERMINATION_VALUES') if observation else None,
            'obs_rel_stat_spon_id': self.get_lobelia_value(observation['obs_rel_statut_spon_id'], 'STATUT_SPONTANEITE_VALUES') if observation else None,
            'obs_rel_note_ident': None,
            'obs_rel_effectif': None,
            'obs_rel_surf': None,
            'obs_rel_confidentiel_on': 0 if observation else None,
            'obs_rel_cd_nom': observation['cd_nom'] if observation else None,
            'obs_rel_classe_surf_id': self.get_lobelia_value(observation['obs_rel_classe_surfaces_id'], 'CLASSE_SURFACES_VALUES') if observation else None,
            'obs_rel_classe_eff_id': self.get_lobelia_value(observation['obs_rel_classe_effectifs_id'], 'CLASSE_EFFECTIFS_VALUES') if observation else None,
            'obs_rel_id_origine': f"L4Q:{observation['obs_id']}" if observation else None,
            'obs_rel_effectif_max': None,
            'obs_rel_obj_denomb_id': None,
            'obs_rel_uuid': observation['obs_uuid'] if observation else None,
            'obs_rel_presence': 1 if observation else None,
            'obs_rel_confidentiel': 0 if observation else None,
            'obs_rel_support': self.get_lobelia_value(observation['obs_rel_support_id'], 'SUPPORT_VALUES') if observation else None,
            'obs_rel_determination_rq': None,
            'obs_rel_taxon_cite_auteur': None,
            'obs_rel_indice_agregation_id': self.get_lobelia_value(observation['obs_rel_indice_aggreg_id'], 'INDICE_AGREGATION_VALUES') if observation else None,
            'obs_rel_denombrement_methode_id': None,
            'obs_rel_struct_pop_rq': None,
            'obs_rel_effectif_rq': None,
            'obs_rel_page_biblio': None,
            'obs_rel_indigenat_id': None,
            'coeff_phyto': json.dumps(coeff_phyto, ensure_ascii=False) if coeff_phyto else coeff_phyto,
            'phenologies': json.dumps(phenologies, ensure_ascii=False) if phenologies else phenologies,
            'determinateurs': None,
            'obs_id_interne_valide': None,
            'note_valide': None,
            'commentaire_valide': None,
            'org_id_interne_valide': None,
            'type_valid_id': None,
            'date_validation': None
        }

    def build_obs_habitat_csv(self) -> bool:
        # Get SQLite data of obs_habitat
        obs_habitat_list = self.synchro_cur.execute(f"""WITH real_obs_habitats AS (
            SELECT *
            FROM obs_habitats
            WHERE NOT (obs_hab_rel_id IS NULL AND obs_hab_rel_assoc_id IS NOT NULL and obs_hab_code_typo == 0 AND obs_hab_code_cite IS NULL)
        ), real_liens_obs_habitat AS (
            SELECT *
            FROM liens_obs_habitat
            WHERE id_obs_hab_1 IN (SELECT obs_hab_id FROM real_obs_habitats) AND id_obs_hab_2 IN (SELECT obs_hab_id FROM real_obs_habitats)
        )
        SELECT obs_hab_id, obs_hab_uuid, obs_hab_rel_id, 
        obs_hab_rel_assoc_id, obs_hab_code_typo, obs_hab_code_cite, obs_hab_nom_cite, obs_hab_forme_id,
        obs_hab_recouvrement, obs_hab_rec_pct, syntaxon_typicite_flor_id, syntaxon_typicite_struct_id, 
        comm_desc_gen, obs_hab_type_deter_id, obs_hab_commentaire_habitat, syntaxon_comm_non_sat_id, 
        syntaxon_comm_non_sat_taxon_dom, syntaxon_comm_non_sat_taxon_dom_nom, rloh.obs_hab_relations_id
        FROM real_obs_habitats roh
        LEFT JOIN (
            SELECT id_obs_hab_1, JSON_GROUP_ARRAY(id_obs_hab_2) AS obs_hab_relations_id
            FROM real_liens_obs_habitat rloh
            GROUP BY id_obs_hab_1
        ) rloh ON rloh.id_obs_hab_1 = roh.obs_hab_id""").fetchall()
        # For each obs_habitat...
        rows = []
        for obs_hab in obs_habitat_list:
            # Save data
            formatted_obs_habitat = self.get_formatted_obs_habitat_dict(obs_hab)
            if formatted_obs_habitat:
                rows.append(formatted_obs_habitat)
        rows.append(self.get_formatted_obs_habitat_dict())  # Csv must be created even if L4Q project does not contain any obs_habitat
        # Write csv
        with open(str(self.csv_dir / OBS_HABITAT_CSV_NAME), 'w', encoding='utf-8', newline='') as output_file:
            fieldnames = self.get_fieldnames(rows[0])
            writer = csv.DictWriter(output_file, fieldnames=fieldnames, delimiter=';')
            writer.writeheader()
            rows = rows[:-1]  # Delete useless empty row
            writer.writerows(rows)
        print('obs_habitat.csv OK')
        return True

    def get_formatted_obs_habitat_dict(self, obs_habitat=None):
        occ_hab_validation = self.sync_dialog.occ_hab_validation.currentData()
        return {
            'id_interne': obs_habitat['obs_hab_id'] if obs_habitat else None,
            'occ_hab_id': None,
            'occ_hab_uuid': obs_habitat['obs_hab_uuid'] if obs_habitat else None,
            'occ_hab_id_origine': f"L4Q:{obs_habitat['obs_hab_id']}" if obs_habitat else None,
            'occ_hab_rel_id_interne': obs_habitat['obs_hab_rel_id'] if obs_habitat else None,
            'occ_hab_rel_assoc_id_interne': obs_habitat['obs_hab_rel_assoc_id'] if obs_habitat else None,
            'occ_hab_date': date.today() if obs_habitat else None,
            'occ_hab_cd_typo': (992 if obs_habitat['obs_hab_code_typo'] == 0 else obs_habitat['obs_hab_code_typo']) if obs_habitat else None,
            'determination_code_habitat': (obs_habitat['obs_hab_code_cite'] if obs_habitat['obs_hab_code_typo'] != 0 else None) if obs_habitat else None,
            'determination_code_phyto': (obs_habitat['obs_hab_code_cite'] if obs_habitat['obs_hab_code_typo'] == 0 else None) if obs_habitat else None,
            'determination_nom_cite': obs_habitat['obs_hab_nom_cite'] if obs_habitat else None,
            'occ_hab_presence': 1 if obs_habitat else None,
            'occ_hab_hic_prio': None,
            'occ_hab_saisi_obs_id_interne': self.contact[0] if obs_habitat else None,
            'occ_hab_forme': self.get_lobelia_value(obs_habitat['obs_hab_forme_id'], 'TYPE_FORME_VALUES') if obs_habitat else None,
            'occ_hab_rec_coeff': self.get_lobelia_value(obs_habitat['obs_hab_recouvrement'], 'RECOUVREMENT_VALUES') if obs_habitat else None,
            'occ_hab_rec_pct': (int(obs_habitat['obs_hab_rec_pct']) if obs_habitat['obs_hab_rec_pct'] is not None and obs_habitat['obs_hab_rec_pct'] != '' else None) if obs_habitat else None,
            'occ_hab_typicite_flor': self.get_lobelia_value(obs_habitat['syntaxon_typicite_flor_id'], 'TYPICITE_VALUES') if obs_habitat else None,
            'occ_hab_typicite_struct': self.get_lobelia_value(obs_habitat['syntaxon_typicite_struct_id'], 'TYPICITE_VALUES') if obs_habitat else None,
            'occ_hab_commentaire': obs_habitat['comm_desc_gen'] if obs_habitat else None,
            'determination_methode': self.get_lobelia_value(obs_habitat['obs_hab_type_deter_id'], 'TYPE_DETERMINATION_VALUES') if obs_habitat else None,
            'determination_fiabilite': None,
            'determination_stat_det': None,
            'determination_rq': obs_habitat['obs_hab_commentaire_habitat'] if obs_habitat else None,
            'infos_phyto_com_non_sat': self.get_lobelia_value(obs_habitat['syntaxon_comm_non_sat_id'], 'COMM_NON_SATUREES_VALUES') if obs_habitat else None,
            'infos_phyto_cd_nom_taxon_dom': obs_habitat['syntaxon_comm_non_sat_taxon_dom'] if obs_habitat else None,
            'infos_phyto_nom_cite_taxon_dom': obs_habitat['syntaxon_comm_non_sat_taxon_dom_nom'] if obs_habitat else None,
            'infos_phyto_diag_originale': None,
            'validation_note': ('Probable' if occ_hab_validation == 1 else None) if obs_habitat else None,
            'validation_type_valid': ('Régionale' if occ_hab_validation == 1 else None) if obs_habitat else None,
            'validation_obs_id_interne': (self.sync_dialog.validator.currentData()[0] if occ_hab_validation == 1 else None) if obs_habitat else None,
            'validation_org_id_interne': (1 if occ_hab_validation == 1 else None) if obs_habitat else None,
            'validation_date': (date.today() if occ_hab_validation == 1 else None) if obs_habitat else None,
            'validation_commentaire': (self.sync_dialog.validation_comment.toPlainText() if occ_hab_validation == 1 and self.sync_dialog.validation_comment.toPlainText() else None) if obs_habitat else None,
            'determinateurs': None,
            'occ_hab_relations_id_interne': obs_habitat['obs_hab_relations_id'] if obs_habitat else None
        }

    def build_all_csv(self):
        # TODO : Add comments !!!
        try:
            self.save_synchro_information()
            if not self.create_import_directory():
                self.commit_and_close_connections()
                return
            if len(self.empty_surveys) > 0:
                self.remove_empty_surveys()
            if len(self.surveys_without_geom) > 0:
                self.remove_surveys_without_geom()
            self.jdd_uuid_list = [row[0] for row in self.synchro_cur.execute('SELECT DISTINCT rel_jdd_uuid FROM releves')]
            if self.build_source_csv() and self.build_observers_csv() and self.build_organizations_csv() \
                    and self.build_datasets_csv() and self.build_frameworks_csv() \
                    and self.build_surveys_csv() and self.build_observations_csv() and self.build_obs_habitat_csv():
                self.synchro_conn.commit()
                log_type = self.translator.tr('Synchronization')
                log = self.translator.tr('Synchronization has been successfull !')
                self.iface.messageBar().pushSuccess(log_type, log)
                self.logs_to_save.append({'log_type': log_type, 'log': log})
                self.commit_and_close_connections(success=True)
            else:
                self.commit_and_close_connections()
        except Exception as e:
            self.catch_exception(e)
            raise
        # except sqlite3.Error as e:
        #     self.catch_exception(e)
        #     raise
        except:
            self.catch_exception()
            raise

    def catch_exception(self, e=None):
        error_type = self.translator.tr('QGIS/Python error')
        error = self.translator.tr('Synchronization has not been successfull')
        self.logs_to_save.append({'log_type': error_type, 'log': str(e) if e else 'Unexpected error'})
        self.iface.messageBar().pushWarning(error_type, error)
        self.logs_to_save.append({'log_type': error_type, 'log': error})
        self.commit_and_close_connections()

    def commit_and_close_connections(self, success=False):
        self.synchro_conn.close()
        for log in self.logs_to_save:
            self.save_log(log['log_type'], log['log'])
        if success:
            self.sqlite_cur.execute('UPDATE synchro SET success=1;')
            self.sqlite_conn.commit()
        else:
            self.remove_csv()
        self.sqlite_conn.close()
        self.pg_conn.close()

    def remove_csv(self):
        """In case of error, remove all csv from the server as well as the import directory."""
        if self.csv_dir and self.csv_dir.exists():
            filelist = [file for file in self.csv_dir.iterdir() if str(file).endswith('.csv')]
            for file in filelist:
                file.unlink()
            self.csv_dir.rmdir()

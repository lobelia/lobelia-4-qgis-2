import os

from PyQt5.QtCore import QTranslator, QCoreApplication, QSettings


class Translator(QTranslator):
    def __init__(self, context: str = 'Lobelia4QGIS', locale: str = None, locale_path: str = None):
        super().__init__()
        self.locale = locale
        self.locale_path = locale_path
        self.translator_dir = os.path.dirname(__file__)
        self.translator = None
        self.context = context

        # Initialize locale
        if self.locale is None:
            self.locale: str = QSettings().value('locale/userLocale')[0:2]
        if self.locale_path is None:
            self.locale_path = os.path.join(self.translator_dir, '{}.qm'.format(self.locale))

        if os.path.exists(self.locale_path):
            self.translator = QTranslator()
            self.translator.load(self.locale_path)
            QCoreApplication.installTranslator(self.translator)

    def tr(self, message: str, **kwargs) -> str:
        """
        Get the translation for a string using Qt translation API.
        """
        if self.translator is not None:
            return self.translator.translate(self.context, message)
        else:
            return message

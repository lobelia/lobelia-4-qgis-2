<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr" sourcelanguage="en">
    <context>
        <name>Lobelia4QGIS</name>
        <message>
            <source>Synchronize to the Lobelia DB</source>
            <translation>Synchroniser vers la BDD Lobelia</translation>
        </message>
        <message>
            <source>Update datasets list</source>
            <translation>Mettre à jour la liste des JDD</translation>
        </message>
        <message>
            <source>Update observers list</source>
            <translation>Mettre à jour la liste des observateurs</translation>
        </message>
        <message>
            <source>Update station_surface layer from carto_habitats.shp</source>
            <translation>Mettre à jour la couche station_surface depuis carto_habitats.shp</translation>
        </message>
        <message>
            <source>SQLite DB copy could not have been created</source>
            <translation>La copie de la base SQLite n'a pas pu être créée</translation>
        </message>
        <message>
            <source>Lobelia database connection</source>
            <translation>Connexion à la BDD Lobelia</translation>
        </message>
        <message>
            <source>No matching PostGIS connection to "{0}"</source>
            <translation>Aucune connexion PostGIS ne correspond à "{0}". Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Connection to "{0}" database could not be made</source>
            <translation>La connexion à la base de données "{0}" n'a pas pu être établie. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Empty password !</source>
            <translation>Aucun mot de passe renseigné !</translation>
        </message>
        <message>
            <source>Invalid login</source>
            <translation>Les identifiants fournis ne sont pas valides</translation>
        </message>
        <message>
            <source>Connection to "{0}" database has successfully been created !</source>
            <translation>La connexion à la base de données "{0}" a bien été créée !</translation>
        </message>
        <message>
            <source>SQLite DB</source>
            <translation>BDD SQLite</translation>
        </message>
        <message>
            <source>The QGIS project does not have any SQLite DB ("donnees_habitats.sqlite" file does not exist)</source>
            <translation>Le projet QGIS ne comporte pas de BDD SQLite (le fichier "donnees_habitats.sqlite" n'existe pas)</translation>
        </message>
        <message>
            <source>Connection to SQLite DB could not be made</source>
            <translation>La connexion à la BDD SQLite n'a pas pu être établie. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Synchronization</source>
            <translation>Synchronisation</translation>
        </message>
        <message>
            <source>QGIS/Python error</source>
            <translation>Erreur QGIS/Python</translation>
        </message>
        <message>
            <source>Synchronization has not been successfull</source>
            <translation>La synchronisation des données au format Lobelia (csv) n'a pas abouti</translation>
        </message>
        <message>
            <source>Synchronization has been successfull !</source>
            <translation>La synchronisation des données au format Lobelia (csv) a réussi !</translation>
        </message>
        <message>
            <source>Table "{0}" does not exist in the SQLite DB</source>
            <translation>La table "{0}" n'existe pas dans la BDD SQLite. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>There is no survey in this QGIS project</source>
            <translation>Il n'y a aucun relevé dans ce projet QGIS</translation>
        </message>
        <message>
            <source>CRS of the layer {0} is neither 'EPSG:2154' nor 'EPSG:4326'. Please change the CRS of the layer.</source>
            <translation>Le SCR (système de coordonnées de référence) de la couche {0} n'est pas de type 'EPSG:2154' ni 'EPSG:4326'. Veuillez modifier la projection de la couche.</translation>
        </message>
        <message>
            <source>Following geometries are not valid and must be corrected : </source>
            <translation>Les géométries suivantes ne sont pas valides et doivent être corrigées : </translation>
        </message>
        <!-- <message>
            <source>Survey id={0} is linked to a point uuid that does not exist</source>
            <translation>Le relevé id={0} est relié à un uuid de type point qui n'existe pas. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Survey id={0} is linked to a line uuid that does not exist</source>
            <translation>Le relevé id={0} est relié à un uuid de type ligne qui n'existe pas. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Survey id={0} is linked to a polygon uuid that does not exist</source>
            <translation>Le relevé id={0} est relié à un uuid de type polygone qui n'existe pas. Veuillez contacter le Pôle SI.</translation>
        </message> -->
        <message>
            <source>Survey id={0} is not linked to any geometry</source>
            <translation>Le relevé id={0} n'a pas de géométrie. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Survey id={0} is not linked to any observer</source>
            <translation>Le relevé id={0} n'a pas d'observateur</translation>
        </message>
        <message>
            <source>Survey id={0} is not linked to any dataset</source>
            <translation>Le relevé id={0} n'est pas relié à un jeu de données</translation>
        </message>
        <message>
            <source>Survey id={0} is not linked to any date</source>
            <translation>Le relevé id={0} n'a pas de date</translation>
        </message>
        <!-- <message>
            <source>Survey id={0} is not linked to any observation or obs_habitat</source>
            <translation>Le relevé id={0} n'a pas d'observation ni d'obs_habitat (ou bien l'obs_habitat est vide)</translation>
        </message> -->
        <message>
            <source>Unique observer {0} has been added for survey id={1}</source>
            <translation>L'observateur unique {0} a été ajouté pour le relevé id={1}</translation>
        </message>
        <message>
            <source>Unique dataset "{0}" has been added for survey id={1}</source>
            <translation>Le JDD unique "{0}" a été ajouté pour le relevé id={1}</translation>
        </message>
        <message>
            <source>Observation obs_id={0} is not linked to any survey</source>
            <translation>L'observation obs_id={0} n'est reliée à aucun relevé. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Obs_habitat obs_hab_id={0} is not linked to any survey</source>
            <translation>L'obs_habitat obs_hab_id={0} n'est reliée à aucun relevé. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Observation obs_id={0} is linked to flora survey id={1} that does not exist</source>
            <translation>L'observation obs_id={0} est relié au relevé flore id={1} qui n'existe pas. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Obs_habitat obs_hab_id={0} is linked to flora survey id={1} that does not exist</source>
            <translation>L'obs_habitat obs_hab_id={0} est relié au relevé flore id={1} qui n'existe pas. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Obs_habitat obs_hab_id={0} is linked to habitat survey id={1} that does not exist</source>
            <translation>L'obs_habitat obs_hab_id={0} est relié au relevé habitat id={1} qui n'existe pas. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Observation obs_id={0} (survey id={1}) does not have nom_cite</source>
            <translation>L'observation obs_id={0} (relevé id={1}) n'a pas de nom_cite</translation>
        </message>
        <message>
            <source>Obs_habitat obs_hab_id={0} (survey id={1}) does not have nom_cite</source>
            <translation>L'obs_habitat obs_hab_id={0} (relevé id={1}) n'a pas de nom_cite</translation>
        </message>
        <message>
            <source>Obs_habitat obs_hab_id={0} (survey id={1}) does not have code_typo</source>
            <translation>L'obs_habitat obs_hab_id={0} (relevé id={1}) n'a pas de code_typo. Veuillez contacter le Pôle SI.</translation>
        </message>
        <!-- <message>
            <source>Observation obs_id={0} does not have cd_nom => It may need to be reindexed !</source>
            <translation>L'observation obs_id={0} n'a pas de cd_nom => Elle nécessite peut-être d'être réindexée !</translation>
        </message> -->
        <message>
            <source>The format of some dates into the "releves" table had to be updated</source>
            <translation>Le format de certaines dates de la table "releves" a dû être modifié</translation>
        </message>
        <message>
            <source>Survey id={0} refers a syntaxon through rel_obs_syntaxon_id={1} that does not exist</source>
            <translation>Le relevé id={0} fait référence à un syntaxon via rel_obs_syntaxon_id={1} qui n'existe pas. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>For obs_habitat obs_hab_id={0}, obs_hab_rel_assoc_id should be equal to {1}</source>
            <translation>Pour l'obs_habitat obs_hab_id={0}, obs_hab_rel_assoc_id devrait être égal à {1}. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Modifications has been made to obs_habitats table</source>
            <translation>Des modifications ont été apportées à la table obs_habitats</translation>
        </message>
        <message>
            <source>Form</source>
            <translation>Formulaire</translation>
        </message>
        <message>
            <source>Error</source>
            <translation>Erreur</translation>
        </message>
        <message>
            <source>Matching error</source>
            <translation>Erreur de correspondance</translation>
        </message>
        <message>
            <source>Servor path does not exist</source>
            <translation>Le chemin vers le serveur n'existe pas</translation>
        </message>
        <message>
            <source>This directory name already exists on the servor</source>
            <translation>Un dossier avec le même nom existe déjà sur le serveur</translation>
        </message>
        <message>
            <source>Empty values !</source>
            <translation>Certaines valeurs attendues sont vides !</translation>
        </message>
        <message>
            <source>Key {0} has not been found in {1} SQLite table for list {2}</source>
            <translation>La clé {0} n'a pas été trouvée dans la table SQLite {1} pour la liste {2}. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>L4Q observer {0} has not been found in the Lobelia DB</source>
            <translation>L'observateur L4Q {0} n'a pas été trouvé dans la BDD Lobelia. Veuillez mettre à jour la liste des observateurs ou contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>L4Q observer {0} has been found more than once in the Lobelia DB</source>
            <translation>L'observateur L4Q {0} a été trouvé plusieurs fois dans la BDD Lobelia. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>L4Q organization "{0}" has not been found in the Lobelia DB</source>
            <translation>L'organisme L4Q "{0}" n'a pas été trouvé dans la BDD Lobelia. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>L4Q dataset "{0}" has not been found in the Lobelia DB</source>
            <translation>Le jeu de données L4Q "{0}" n'a pas été trouvé dans la BDD Lobelia. Veuillez mettre à jour la liste des JDD ou contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>L4Q dataset "{0}" has been found more than once in the Lobelia DB</source>
            <translation>Le jeu de données L4Q "{0}" a été trouvé plusieurs fois dans la BDD Lobelia. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>L4Q framework has not been found in the Lobelia DB</source>
            <translation>Le cadre d'acquisition L4Q n'a pas été trouvé dans la BDD Lobelia. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Survey id={0} could not have been intersected with any municipality of the Lobelia DB</source>
            <translation>Le relevé id={0} ne se trouve dans aucune commune de la BDD Lobelia. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Datasets update</source>
            <translation>Mise à jour des JDD</translation>
        </message>
        <message>
            <source>Datasets have successfully been updated !</source>
            <translation>Les JDD ont bien été mis à jour !</translation>
        </message>
        <message>
            <source>Datasets update has not been successfull</source>
            <translation>La mise à jour des JDD n'a pas abouti. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>Observers update</source>
            <translation>Mise à jour des observateurs</translation>
        </message>
        <message>
            <source>Observers update is impossible because observers have already been entered !</source>
            <translation>La mise à jour des observateurs est impossible car des observateurs ont déjà été saisis !</translation>
        </message>
        <message>
            <source>Observers have successfully been updated !</source>
            <translation>Les observateurs ont bien été mis à jour !</translation>
        </message>
        <message>
            <source>Observers update has not been successfull</source>
            <translation>La mise à jour des observateurs n'a pas abouti. Veuillez contacter le Pôle SI.</translation>
        </message>
        <message>
            <source>station_surface update</source>
            <translation>Mise à jour de station_surface</translation>
        </message>
        <message>
            <source>station_surface has successfully been updated !</source>
            <translation>La couche station_surface a bien été mise à jour !</translation>
        </message>
        <message>
            <source>station_surface update has not been successfull</source>
            <translation>La mise à jour de la couche station_surface n'a pas abouti</translation>
        </message>
        <message>
            <source>carto_habitats layer has not been found in the QGIS project</source>
            <translation>La couche Shapefile carto_habitats n'a pas été trouvée dans le projet QGIS</translation>
        </message>
        <message>
            <source>station_surface entity id={0} has no equivalent polygon in carto_habitats !</source>
            <translation>L'entité station_surface id={0} n'a pas de polygone équivalent dans carto_habitats !</translation>
        </message>
        <message>
            <source>station_surface entity id={0} straddles several polygons of carto_habitats : id = {1}</source>
            <translation>L'entité station_surface id={0} est à cheval sur plusieurs polygones carto_habitats : id = {1}</translation>
        </message>
        <message>
            <source>carto_habitats polygon id={0} contains several station_surface entities : id = {1}</source>
            <translation>Le polygone carto_habitats id={0} contient plusieurs entités station_surface : id = {1}</translation>
        </message>
        <message>
            <source>carto_habitats polygon id={0} contains no station_surface entity</source>
            <translation>Le polygone carto_habitats id={0} ne contient aucune entité station_surface</translation>
        </message>
    </context>
    <context>
        <name>PostgisConnectionLoginDialogBase</name>
        <message>
            <source>Lobelia4QGIS - Lobelia DB login</source>
            <translation>Lobelia4QGIS - Identifiants BDD Lobelia</translation>
        </message>
        <message>
            <source>Please enter connection login to Lobelia database.</source>
            <translation>Veuillez renseigner les identifiants de connexion à la base de données Lobelia.</translation>
        </message>
        <message>
            <source>User :</source>
            <translation>Utilisateur :</translation>
        </message>
        <message>
            <source>Password :</source>
            <translation>Mot de passe :</translation>
        </message>
    </context>
    <context>
        <name>SuccessfullSynchroDialogBase</name>
        <message>
            <source>Lobelia4QGIS - Data already synchronized</source>
            <translation>Lobelia4QGIS - Données déjà synchronisées</translation>
        </message>
        <message>
            <source>Caution! This project data has already been synchronized to the Lobelia format (csv files). Are you sure you want to continue?</source>
            <translation>Attention ! Les données de ce projet L4Q ont déjà été synchronisées au format Lobelia (fichiers csv). Êtes-vous sûr(e) de vouloir continuer ?</translation>
        </message>
    </context>
    <context>
        <name>SynchronizeDataDialogBase</name>
        <message>
            <source>Lobelia4QGIS - Synchronize to the Lobelia DB</source>
            <translation>Lobelia4QGIS - Synchroniser vers la BDD Lobelia</translation>
        </message>
        <message>
            <source>Data saving to the Lobelia format (csv files) on the server</source>
            <translation>Sauvegarde des données au format Lobelia (fichiers csv) sur le serveur</translation>
        </message>
        <message>
            <source>Server root path :</source>
            <translation>Chemin vers la racine du serveur "N" :</translation>
        </message>
        <message>
            <source>NB: If the server root path is not editable, this means it is correct. If not, please enter the correct path.</source>
            <translation>NB : Si le chemin vers la racine du serveur "N" n'est pas modifiable, cela signifie qu'il est correct. Dans le cas contraire, veuillez renseigner le bon chemin.</translation>
        </message>
        <message>
            <source>Description of the data import</source>
            <translation>Description de l'import de données</translation>
        </message>
        <message>
            <source>Import name :</source>
            <translation>Nom de l'import :</translation>
        </message>
        <message>
            <source>Description :</source>
            <translation>Description :</translation>
        </message>
        <message>
            <source>Point of contact :</source>
            <translation>Référent :</translation>
        </message>
        <message>
            <source>Data validation</source>
            <translation>Validation des données</translation>
        </message>
        <message>
            <source>Have the habitat occurrences been validated in accordance with the internal validation protocol?</source>
            <translation>Les occurrences habitats ont-elles été validées conformément au protocole interne de validation ?</translation>
        </message>
        <message>
            <source>Validation details</source>
            <translation>Infos validation</translation>
        </message>
        <message>
            <source>Validator :</source>
            <translation>Validateur :</translation>
        </message>
        <message>
            <source>Comment (optional) :</source>
            <translation>Commentaire (optionnel) :</translation>
        </message>
        <message>
            <source>You are about to synchronize your project data to the Lobelia format (csv). Pending import into the Lobelia DB, these csv files will be stored on the server, in the folder mentioned just above.</source>
            <translation>Vous vous apprêtez à synchroniser les données de votre projet L4Q au format Lobelia sous forme de fichiers csv. Dans l'attente d'un import vers la BDD Lobelia, ces fichiers csv seront stockés sur le serveur "N", dans le dossier indiqué plus haut.</translation>
        </message>
        <message>
            <source>Are you sure you want to continue?</source>
            <translation>Êtes-vous sûr(e) de vouloir continuer ?</translation>
        </message>
    </context>
</TS>

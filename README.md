# Lobelia 4 QGIS 2

**Lobelia 4 QGIS 2** est un plugin QGIS destiné, à terme, à remplacer le projet QGIS 
[Lobelia 4 QGIS](https://gitlab.com/lobelia/lobelia-4-qgis). Il permet de :

- **Synchroniser les données d'un projet Lobelia 4 QGIS au format Lobelia**, sous forme de fichiers csv. Ces fichiers 
csv exportés sont formatés de sorte à être intégrés dans la base Lobelia via le programme 
d'[intégration pivot](https://gitlab.com/lobelia/integration-pivot).
- **Mettre à jour la liste des JDD** d'un projet Lobelia 4 QGIS depuis la base Lobelia de calcul.
- **Mettre à jour la liste des observateurs** d'un projet Lobelia 4 QGIS depuis la base Lobelia de calcul.

Bientôt, il récupèrera les fonctions de **saisie de relevés floristiques ou d'habitats au format Lobelia**, déjà 
présentes dans le projet Lobelia 4 QGIS.

*La structure de ce plugin a été générée via Plugin Builder 3 for QGIS.*

## Pré-requis
- Version de QGIS utilisée : 3.34 LTR
- Version de python utilisée pour le développement : 3.10
- BDD Lobelia :
  - **User** [`qgis_plugins_for_lobelia`](https://gitlab.com/lobelia/lobelia-migration/-/blob/main/CBNBP/calcul/create_v_for_lobelia_user_in_qgis_plugins.sql)
  - **Vues matérialisées** [`cbn_v.vm_observations_with_geom`](https://gitlab.com/lobelia/lobelia-migration/-/blob/main/CBNBP/calcul/create_vm_observations_with_geom.sql), [`cbn_v.vm_obs_habitats_with_geom`](https://gitlab.com/lobelia/lobelia-migration/-/blob/main/CBNBP/calcul/create_vm_obs_habitats_with_geom.sql) et [`cbn_v.vm_cbnbp_grille_maille_1km`](https://gitlab.com/lobelia/lobelia-migration/-/blob/main/CBNBP/calcul/create_vm_cbnbp_grille_maille_1km.sql)
  - **[Vues](https://gitlab.com/lobelia/lobelia-migration/-/blob/main/CBNBP/calcul/create_v_for_lobelia_user_in_qgis_plugins.sql)** `cbn_v.v_user`, `cbn_v.v_observateurs` et `cbn_v.v_organismes` 

## Installation
Vous trouverez toutes les informations nécessaires dans le [Wiki du dépôt](https://gitlab.com/lobelia/lobelia_4_qgis_2/-/wikis/home#installation-du-plugin-qgis-lobelia4qgis).

## Utilisation
Vous trouverez toutes les informations nécessaires dans le [Wiki du dépôt](https://gitlab.com/lobelia/lobelia_4_qgis_2/-/wikis/home#utilisation-du-plugin-qgis-lobelia4qgis).
